/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Bluetooth PAL-layer module.
 *
 * This module provides connections to Bluetooth devices.
 *
 * Currently, this module only provides access to HDP devices, but in the
 * future, support for BLE ("Smart") devices and device management operations
 * (scanning, pairing etc.) should be added to this module.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute, &copy; 2014
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef BLUETOOTH_H
#define BLUETOOTH_H

#include "personalhealthdevice.h"
#include "FSYS/msgreceiver.h"
#include "4SDC/core.h"
#include "FSYS/moduledeclare.h"





namespace PAL {

/**
 * @brief Bluetooth PAL-layer module wrapper class
 *
 * This class wraps a module providing connections to classic Bluetooth (Health
 * Device Profile) devices. In the future, support for BLE devices and device
 * management operations should be be added to this class as well.
 *
 * The class derives from the PAL::PHDBluetoothProvider which handles the
 * HDP communication with other modules.
 *
 * @todo Revisit documentation for instructions on starting and terminating the
 *       module using the module launcher
 * @see PAL::PHDBluetoothProvider
 */
class BluetoothModule : public PAL::PHDBluetoothProvider,
                        public FSYS::Module {

public:

    /**
     * @brief Launch and initialize the Bluetooth module.
     *
     * The Bluetooth module is initialized.
     *
     * @todo Revisit documentation for instructions on starting and terminating
     *       the module using the module launcher
     */
    BluetoothModule();

    /**
     * @brief Free resources used by the Bluetooth module.
     */
    ~BluetoothModule();

    /**
     * @brief Will stop the module and (relatively gracefully) disconnect any
     *        open connections.
     *
     * This method stops the Bluetooth module. If any connections are open, they
     * will be disconnected with "gentle force". This signal should be delivered
     * to the module just before it is destructed. When the module receives
     * this signal, it will stop accepting new connections and close all
     * existing - in other words: it will be a brick waiting for destruction.
     * @param signal The Terminate signal.
     */
    void msgGoingDown();



    /* ************************************************************ */
    /*     PHDBluetoothProvider (communicating with HDP devices)    */
    /* ************************************************************ */

    /**
     * @brief A session-layer component has been registered as handler of a
     *        datatype.
     *
     * This method is called when a session-layer component is ready to handle
     * the given datatype. We should start accepting incoming connections of
     * this type.
     *
     * @param datatype The datatype that will be handled.
     */
    void registerDatatype(uint16_t datatype) noexcept;

    /**
     * @brief Session-layer components are no longer registered to handle this
     *        datatype.
     *
     * This method is called when no more session-layer components are ready to
     * handle the given datatype. We should no longer accept incoming
     * connections of this type.
     *
     * @param datatype The datatype that will no longer be accepted.
     */
    void unregisterDatatype(uint16_t datatype) noexcept;

    /**
     * @brief Send a message to this device on the primary channel.
     *
     * Send a message to the given device on the reliable primary channel. If
     * no channel is currently open to this device, ignore the call; if the
     * transmission fails (e.g. because the destination went out of range),
     * follow up with a disconnectIndication() with a suitable error code.
     *
     * @param device The destination
     * @param apdu   The message payload
     */
    void sendApduPrimary(std::shared_ptr<PAL::VirtualPHD> device,
                         std::shared_ptr<std::vector<uint8_t> > apdu) noexcept;

    /**
     * @brief Send a message to this device on a streaming channel.
     *
     * Attempt to send this message to the given device on an unreliable
     * streaming channel. If no streaming channel is currently open, use the
     * primary channel instead (@see sendApduPrimary()).
     *
     * @param device The destination
     * @param apdu   The message payload
     */
    void sendApduStreaming(std::shared_ptr<PAL::VirtualPHD> device,
                          std::shared_ptr<std::vector<uint8_t> > apdu) noexcept;

    /**
     * @brief Session-layer component request to disconnect a device.
     *
     * The session-layer component handling this device wishes to disconnect.
     * When the device has been disconnected, we must reply with a
     * disconnectIndication().
     *
     * @param device The device we must disconnect.
     */
    void disconnect(std::shared_ptr<PAL::VirtualPHD> device) noexcept;

private:
    // The internal parts of this class
    class PrivateBluetoothModule;
    friend class PrivateBluetoothModule;
    PrivateBluetoothModule *privateParts;

};

} // namespace PAL

#endif // BLUETOOTH_H

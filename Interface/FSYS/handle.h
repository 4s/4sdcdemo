/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains interface declaration for the FSYS::Handle class
 *
 * /see FSYS::Handle
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
#ifndef HANDLE_H
#define HANDLE_H

#include <cstdint>

namespace FSYS
{
  /**
   * @brief Class that holds and assigns handles
   *
   * If you instantiate this class with the default constructor then it will
   * represent a unique handle, if you copy the object, the copy will contain
   * the same handle.
   *
   * It is possible to compare handles with the '!=', '==', '<' and '>'
   * operators, you should not rely on any special meaning of the '<' and '>'
   * operators, as future implemenations might change the order of created
   * handles.
   *
   * Two special handels are exposed by the class - the "null" and "broadcast"
   * handles - you can not rely on how these two handles are ordered compared
   * to other handles (e.g. null() will not always compare smaller to other
   * handles, nor will null() compare equal to 0.
   */
  class Handle
  {
    /**
     * @brief The actual value of the handle
     */
    int64_t handle;

    /**
     * @brief Function to generate a unique handle
     *
     * This function will generate a unique handle, it will do this by returning
     * a 64 bit handle - after all ~10¹⁹ handles with a positive value are used
     * the function will assert, if a millon handles are used a second, this is
     * not estimated to happen within the lifetime of a single instance of the
     * application.
     *
     * @return  A unique handle
     */
    int64_t getNextHandle( void );
  public:

    /**
     * @brief Default constructor
     *
     * Using the default constructor will assign a unique handle to this object.
     *
     * If you do not wish to have new handle assigned, then you can get a more
     * efficient program by initializing the handle to the null handle.  This
     * will prevent a global mutex lock to be taken.
     *
     * @code
     * FSYS::Handle myHandle(FSYS::null());
     * .
     * .
     * .
     * myHandle = arg.handle;
     * @endcode
     *
     * is more efficient than:
     *
     * @code
     * FSYS::Handle myHandle;
     * .
     * .
     * .
     * myHandle = arg.handle;
     * @endcode
     *
     * The most efficient is thou:
     *
     * @code
     * FSYS::Handle myHandle(arg.handle)
     * @endcode
     *
     */
    Handle( void ) {handle = getNextHandle();}

    /**
     * @brief Copy constructor for handle object
     * @param ref The handle that should be copied
     */
    Handle( Handle &ref ) : handle(ref.handle) {}

    /**
     * @brief Copy constructor for handle object
     * @param ref The handle that should be copied
     */
    Handle( const Handle &ref ) : handle(ref.handle) {}

    inline Handle& operator= (Handle const &theOtherOne) {handle = theOtherOne.handle; return *this;}

    /**
     * @brief Equality comparator
     *
     * Operator will return true if the two handles are copies of each other
     *
     * @param compareTo The handle this handle should be compared to
     *
     * @return True if the two handles are copies, False if not.
     */
    inline bool operator==(Handle const &compareTo) const
    {
      return (this->handle == compareTo.handle);
    }

    /**
     * @brief Not equalify operator
     *
     * Operator will return true if the two handles are different, e.g. not
     * copies of each other.
     *
     * @param comparTo The handle this handle should be compared to
     *
     * @return
     */
    inline bool operator!=(Handle const &compareTo) const
    {
      return (this->handle != compareTo.handle);
    }

    /**
     * @brief Greater than operator ('>')
     *
     * While handles don't have any meaning relation ship between them, except
     * equality or not equality it is still helpfull for certian algorithms /
     * data structures to be able to order them.
     *
     * You should not rely on any connection between the creation order of the
     * handles and how they compare to each other.
     *
     * The only thing you can rely on is that if we you have three handles
     * A, B and C - and A > B evaluates to true and B > C evaluates to true then
     * A > C will also evaluate to true and in that case B > A, C > B and C > A
     * will also all evaluate to false.
     *
     * @param compareTo The handle that should be compared to this handle
     *
     * @return true or false as descriped in the description of this function
     */
    inline bool operator>(Handle const &compareTo) const
    {
      return (this->handle > compareTo.handle);
    }

    /**
     * @brief Less than operator ('<')
     *
     * @see operator>
     *
     * If the greater than operator ('>') returns true for a relationship
     * between A and B, then this function will return true when the arguments
     * are swapped.  So if A > B evaluates to true, then B < A will also
     * evaluate to true.
     *
     * @param compareTo The handle that this handle should be compared to
     *
     * @return true or false as described in the description of this function
     */
    inline bool operator<(Handle const &compareTo) const
    {
      return (this->handle < compareTo.handle);
    }

    /**
     * @brief Generic broadcast handle
     *
     * The meaning of this handle is specified by its use, from the handle
     * class's point of view, it is just a unique handle.
     *
     * @return The broadcast handle
     */
    static Handle &broadCastHandle( void );

    /**
     * @brief The null handle
     *
     * The null handle is the handle that doesn't represent a valid handle.
     *
     * Two null handles will compare equal to each other, and not equal to other
     * handles.
     *
     * The null handle does not compare equal to the integer value 0, you should
     * consider it equally likely that it compares smaller or greater to any
     * other specific handle, and because it compares smaller to one handle, it
     * doesn't mean that it compares smaller to evey other handle.
     *
     * The actuall value of the handle, will be dependend on a number of factors
     * both compile time and excecution time wise.
     *
     * @return The null handle
     */
    static Handle &null( void );
  };
}

#endif // HANDLE_H

/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains interface declaration for the FSYS::MsgSender class
 *
 * /see FSYS::MsgSender
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
#ifndef MSGSENDER_H
#define MSGSENDER_H

#include "msgsenderbase.h"
#include "msgaddrgenerator.h"

#include <memory>

namespace FSYS
{    
  /**
   * @brief Class enabling other classes to send messages
   *
   * When inheriting from this class it becomes possible for the class
   * inheriting to send all types of messages.
   *
   * There are two ways to send messages, either as broadcasts or as responses
   * to other messages.
   *
   * Broadcasts will be send to all instances of classes that listen to the
   * specific type of message.
   *
   * Responses will only go the the instance of the class that sended the
   * message that is given in the argument to the respond function.
   *
   * For a class to be considered as a receiver of a message, the class needs
   * to be instantiated at the time when the message was sent.
   *
   */
  template<class parentClass> class MsgSender : private MsgSenderBase,
                                                virtual public MsgAddrGenerator<parentClass>
  {
  private:

  public:
    /**
     * @brief Template function to broadcast messages
     *
     * When a message is broadcasted it will be received by all classes
     * listening on the specific message type at the time the message was sent.
     *
     * @warning It is the type of the reference to the message (T in the
     * template class) that is used by the system to identify the type of the
     * message not the actuall message type.  If you have a base class B, and
     * inherit A from it:  B <- A  and you then instantiate an instance of A,
     * but uses a B reference to hold it (like A a; B &b = a;) then the message
     * that will be sent will be of type B.
     * @code
     * class B : public BaseMsg {};
     * class A ; public B {};
     * A a;
     * B &b=a;
     * broadcast(b); // Even this is an A, it will be send as a B, so only
     *               // classes listening for B will get the message
     * @endcode
     *
     * @param msg The message that should be sent
     */
    template<class T> void broadcast(T &msg)
    {
      std::shared_ptr<BaseMsg> pMsg(new T(msg));
      MsgAddr destination = static_cast<MsgAddr>(*this);
      MsgSenderBase::broadcast(pMsg, typeid(T), destination);
    }

    /**
     * @brief Template function to respond to messages
     *
     * When a message is recevied it is possible for at class to send a
     * message back to the sender and only the sender.  To do this you use the
     * respond function.
     *
     * You can use the respond function and another message to send several
     * messages to the sender of that message.
     *
     * A common use case for a flow where the respond function is used is:
     *
     * @code
     *
     * // Client searches for someone to handle a request
     * ServiceXRequest sXr(<arguments to request>);
     * broadcast(sXr);
     * ...
     *
     * // Server receives the request
     * receive(ServiceXRequest &sXr)
     * {
     *    // Server sends ack to the client
     *    ServiceXAck sXa
     *    respond(sXa, sXr);
     *    ...
     *
     *    // At some later point the server can send data to the client
     *    ServiceXProgressIndicator sXpi10(10);
     *    respond(sXpi10, sXr);
     *    ---
     *
     *    // It can send multiple data
     *    ServiceXProgressIndicator xXpi20(20);
     *    respond(sXpi20, sXr);
     *
     * // Client don't need the service of the server any more, so it closes
     * // the connection
     * receive(ServiceXAck sXa)
     * {
     *   ServiceXTerminate sXt;
     *   respond(sXt, sXa);
     *
     * // The service the listsn for the ServiceXTerminate signal, and when
     * // received does what is needed to free internal resources ect.
     *
     * @endcode
     */
    template<class T> void respond(T &msg, BaseMsg &received)
    {
      std::shared_ptr<BaseMsg> pMsg(new T(msg));      
      MsgAddr destination = received.getOriginAddr();
      MsgSenderBase::send(pMsg, typeid(T), *this, destination);
    }

    template<class T> void send(T &msg, const MsgAddr &destination)
    {
      std::shared_ptr<BaseMsg> pMsg(new T(msg));     
      MsgSenderBase::send(pMsg, typeid(T), *this, destination);
    }

  };
}
#endif // MSGSENDER_H

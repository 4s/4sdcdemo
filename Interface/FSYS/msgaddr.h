/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains interface for a message address
 *
 * /see FSYS::MsgAddr
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef MSGADDR_H
#define MSGADDR_H

#include "handle.h"


namespace FSYS
{

  class MsgSenderBase;
  class MsgQueue;
  template<class parentClass> class MsgAddrGenerator;  

/**
 * @brief The MsgAddr class contains a unique address in the message system
 *
 * A MsgAddr identifies a specific instance of a specifc object to the message
 * system.
 *
 */
class MsgAddr
{
  // Friend declarations
  template<class parentClass> friend class MsgAddrGenerator;
  friend class MsgSenderBase;
  friend class MsgQueue;

private:
  /**
   * @brief Handle to the object that this is the address of
   */
  Handle object;

  /**
   * @brief Handle to the queue that serves the object this is the address of
   */
  Handle queue;

  /**
   * @brief The magic key of objecjt this is the address of
   */
  void * magicKey;

  /**
   * @brief Private constructor used for initialization of the object
   * @param object    Handle of the object this addr should refer to
   * @param queue     Queue of the object this addr should refer to
   * @param magicKey  MagicKey of the object this addr should refer to
   */
  MsgAddr(Handle object, Handle queue, void * magicKey);

public:
  /**
   * @brief Default constructor
   *
   * The default constructor doesn't do anything except initializing the private
   * members.
   *
   */
  MsgAddr( void );

  /**
   * @brief Funciton that clears the address stored in the object
   */
  void clear( void );

  /**
   * @brief Function to determine if this address is valid (!=null)
   *
   * This function verifies if the address is null or not, or if part of the
   * address is null - it does not try to determine if the address is actually
   * a valid address of an existing object.
   *
   * @return true if the address is non-null
   *
   * @return false if the addres or part it is null
   *
   */
  bool isValid( void );

  /**
   * @brief Returns the broadcast address
   *
   * The broadcast address is an address that will match all addresses, and
   * hence can be used for sending to all addresses
   *
   * @return The bradcast address
   */
  static MsgAddr &broadcastAddr( void );

  /**
   * @brief operator ==
   *
   * Compares two addresses and return true if they are equal
   *
   * @param compareTo the addres this object should be compared to
   *
   * @return true if the addresses are equal, false if not
   */
  inline bool operator==(MsgAddr const &compareTo) const
  {
    return (this->object == compareTo.object)
        && (this->queue == compareTo.queue)
        && (this->magicKey == compareTo.magicKey);
  }

  /**
   * @brief operator !=
   *
   * Compares two addresses and return false if they are equal
   *
   * @param compareTo the addres this object should be compared to
   *
   * @return false if the addresses are equal, true if not
   */

  inline bool operator!=(MsgAddr const &compareTo) const
  {
    return !(*this == compareTo);
  }
};

}


#endif // MSGADDR_H

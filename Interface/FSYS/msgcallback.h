/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains interface declaration for the FSYS::MsgCallBack class
 *
 * /see FSYS::MsgCallBack
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef MSGCALLBACK_H
#define MSGCALLBACK_H

#include "basemsg.h"
#include "handle.h"

#include <memory>


namespace FSYS
{
  /**
   * @brief The MsgCallBack class is an internal class to the message system
   *
   * @warning Do NOT use this class unless you are coding the message system
   *          it self.  Use the MsgReceiver class instead @see MsgReceiver
   *
   * The MsgCallBack class is the base class that gets registered as listening
   * on a specific message type, it contains a single virtual function which is
   * the one that is called from the queue when the message is taken of the
   * queue.
   */
  class MsgCallBack : virtual public Handle
  {
  public:
    /**
     * @brief Default constructor
     */
    MsgCallBack( void ) {}

    /**
     * @brief Call back function that evantually maps to receive(T)
     *
     * This function is called with a shared pointer to the message that is
     * being processed.
     *
     * @param baseMsg The message that should be forwarded
     */
    virtual void callBack(std::shared_ptr<BaseMsg> &baseMsg) = 0;
  };

  /**
   * Template class that inherits from MsgCallBack
   *
   * The MsgCallBackT class differs from the MsgCallBack class in that it casts
   * the message sent as argument in the callback to the correct type.
   */
  template<class T> class MsgCallBackT : public MsgCallBack
  {
  public:
    /**
     * @brief Callback function with argument of the correct type
     *
     * This is the callback with the message as the correct type
     *
     * @param arg The message that is being sent to this class
     */
    virtual void callBack(T &arg) = 0;

    /**
     * @brief callBack function that casts the message to the correct typedef
     *
     * This function does a type cast of the message to the type given in the
     * template parameter.
     *
     * @param baseMsg The message that is received by the class
     */
    void callBack(std::shared_ptr<BaseMsg> &baseMsg)
    {
      T &msg = static_cast<T&>(*baseMsg);
      callBack(msg);
    }
  };

}

#endif // MSGCALLBACK_H

/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains interface declaration for the FSYS::MsgQueue class
 *
 * /see FSYS::MsgQueue
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef MSGQUEUE_H
#define MSGQUEUE_H

#include "handle.h"
#include "msgaddr.h"
#include <typeinfo>

namespace FSYS
{
  class MsgCallBack;

  /**
   * @brief This is a wrapper class for the class that does all the hard work
   *
   * The MsgQueue class is a wrapper class with the public interface for the
   * message queue.  It contains the functions that gives the caller the ability
   * to:
   *
   * - Register listeners (a listener being the ability for a given instance of
   * a given class to recevie a specific message type).
   *
   * - Unregister listeners (like right before an instance of a class is
   * destroyed)
   *
   * - Checking if the message queue has data for the current thread that needs
   * to be processed
   *
   * - Stopping the current thread until there is a message being sent to it
   *
   * - Stopping the process of emptying the queue, like for breaking out of the
   * internal message loop, say if a class keeps sending messages to it self
   *
   * - Emptying the message queue, either it can start a loop that terminates
   * when the queue is empty, after a given time, or after all the messages that
   * were in the queue at the time of the call has been processed
   *
   */
  class MsgQueue
  {
  public:

    /**
     * @brief Function that registers a listener to the message queue
     *
     * This function registers a listener (the ability of a class to receive a
     * given type of message) with the queue of the thread that the function
     * is called from.
     *
     *
     * @param receiverAddr Address of the receiver
     * @param typeOfMsg
     * @param msgCallBack
     */
    static void addListenerToQueue(MsgAddr &receiverAddr,
                                   const std::type_info &typeOfMsg,
                                   MsgCallBack *msgCallBack);

    /**
     * @brief Function that unregisters a listener from the message queue
     *
     * This function removes a listener from the internal datastructures of the
     * message queue.
     *
     * @param msgCallBack Pointer to the callback that should be removed
     */
    static void removeListenerFromQueue(MsgCallBack *msgCallBack);


    /**
     * @brief Function that halts the current thread execution
     *
     * This function halts the current thread until someone sends a message to
     * the thread.  Be careful not the halt all of your threads, since in that
     * case no-one will be able to send any messages to wake the system up
     * again.
     */
    static void waitUntilQueueHasData( void );

    /**
     * @brief The function will process messages on the message queue
     *
     * This function is used for processing messages from the message queue
     * of the thread that this function is called from.
     *
     * The default behavior is for the function to run until there are no
     * more messages on the queue at which point the function will return.
     *
     * It is possible to give an optional argument to the function limiting
     * the time it will run.
     *
     * @param maxTimeMs  The maximum time in MS that will pass before the
     *                   function breaks the loop that empties the queue.
     *
     *                   maxTimeMs <  0 => Run until the queue is empty or
     *                                     breakEmptyMsgQueue() is called
     *
     *                   maxTimeMS == 0 => Run until all messages on the queue
     *                                     at the time of the call are processed
     *
     *                   maxTimeMS >  0 => Run approximately until maxTimeMS
     *                                     have passed
     */
    static void emptyMsgQueue( int maxTimeMs=-1 );

    /**
     * @brief Breaks out of emptyMsgQueue
     *
     * Function must be called from a message handler (directly or indirectly)
     * and will cause the message loop internal to the emptyMsgQueue function to
     * break.
     *
     * This function is used in the case where you have a component that as part
     * of its operation sends messages to it self.
     *
     * Usually the emptyMsgQueue function will only return when the message is
     * queue is empty, but in the case where components on the queue keeps
     * sending to other components in the same thread, the queue will never be
     * empty and hence will never terminate.
     *
     * Calling this function will break the msg loop.
     */
    static void breakEmptyMsgQueue( void );

    /**
     * @brief Function to determine if there is data on the msg queue
     *
     * Call this function to determine if there are messages waiting to be
     * processed on the queue.
     *
     * @return true if there is messages waiting to be processd, false if not
     */
    static bool isEmpty( void );

    /**
     * @brief Retrieves the handle of the queue for the current thread
     *
     * This function can be used by threads to identify which queue they belong
     * to.
     *
     * @return The handle of the queue for the current thread.
     */
    static Handle getHandle( void );
  };
}
#endif // MSGQUEUE_H

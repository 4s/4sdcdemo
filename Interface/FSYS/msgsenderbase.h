/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains interface declaration for the FSYS::MsgSenderBase class
 *
 * /see FSYS::MsgSenderBase
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
#ifndef MSGSENDERBASE_H
#define MSGSENDERBASE_H

#include "handle.h"
#include "msgaddr.h"

#include <memory>

namespace FSYS
{

  class BaseMsg;

  /**
   * @brief Type neutral message sender class
   *
   * This is the sender base class, the sender class uses the thin template
   * priciple, where the type specific code is concentrated in one class and the
   * type generic code is in a different base class, this is that base class.
   */
  class MsgSenderBase : virtual Handle
  {
  private:
    /**
     * @brief Function that fills out info about the sender of the message
     *
     * This function fills out the sender information, like queue handle,
     * instance handle og magic key of the sender, so a response function later
     * can determine where the result should be sent to.
     *
     * @param msg The message that should have the sender info filled out
     *
     * @param sender The address that should be set as the sender of the message
     *
     * @return A shared pointer to the the message given as parameter
     */
    std::shared_ptr<BaseMsg> &fillOutSenderInfo(std::shared_ptr<BaseMsg> &msg,
                                                MsgAddr sender);

  public:
    /**
     * @brief Broadcast function, to broadcast messages
     *
     * This is a type neutral function that broadcasts a message.  It takes the
     * type info of the message as parameter as well as the message it self.
     *
     * @param msg The message that should be sent.
     *
     * @param typeOfMsg The type (like the typeinfo() type.
     *
     * @param sender Address of the sender of the message
     *
     */
    void broadcast(std::shared_ptr<BaseMsg> &msg,
                   const std::type_info &typeOfMsg,
                   const MsgAddr &sender);

    /**
     * @brief Type independent respond function.
     *
     * This function is used for sending a message back to an instance of a
     * class that previously had sent the "received" message.
     *
     * @param msg The message that should be sent.
     *
     * @param typeOfMsg Type of the message that is being sent
     *
     * @param sender Address of the sender of the message
     *
     * @param destination The destination of the message
     */
    void send(std::shared_ptr<BaseMsg> &msg,
                 const std::type_info &typeOfMsg,
                 const FSYS::MsgAddr &sender,
                 const MsgAddr &destination);
  };
}

#endif // MSGSENDERBASE_H

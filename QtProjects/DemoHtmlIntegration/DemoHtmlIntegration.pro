################################################################################
#                                                                              #
#   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)                   #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
#                                                                              #
################################################################################
#                                                                              #
#   Contributer list:                                                          #
#                                                                              #
#     Jacob Andersen, The Alexandra Institute <jacob.andersen@alexandra.dk>    #
#     Mike Kristoffersen, The Alexandra Institute <4sdc@mikek.dk>              #
#                                                                              #
################################################################################

################################################################################
# Generic build setup
################################################################################


TEMPLATE = app

# Turn on C++11 support
QMAKE_CXXFLAGS = -std=c++11
CONFIG += c++11

# Turn on thread support
LIBS += -pthread

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

INCLUDEPATH += $$PWD/../../Interface

################################################################################
# Files for the QT demo app
################################################################################
RESOURCES += \
    qml.qrc \
    gfx.qrc

# Needed QT libraries
QT += qml widgets quick core

HEADERS += \
    appcontroller.h \
    Gateway/jsongateway.h \
    Gateway/webbrowser.h \    
    Gateway/4SDC2JSON/platformjsongateway.h \
    state.h \
    moduleconfig.h


SOURCES += \
    appcontroller.cpp \
    main.cpp \
    Gateway/4SDC2JSON/jsongateway.cpp \
    Gateway/4SDC2JSON/platformjsongateway.cpp \
    state.cpp


OTHER_FILES += \
    SampleWebPage/index.html

# For all platforms, except Android, we use the QT webkit in the demo app
#_______________________________________________________________________________

!android {
QT += webkit webkitwidgets

RESOURCES += \
    Gateway/4SDC2QtWebKit/browser.qrc

HEADERS += \
    Gateway/4SDC2QtWebKit/platformwebbrowser.h \
    Gateway/4SDC2QtWebKit/webviewgateway.h

SOURCES += \
    Gateway/4SDC2QtWebKit/platformwebbrowser.cpp \
    Gateway/4SDC2QtWebKit/WebBrowser.cpp \
    Gateway/4SDC2QtWebKit/webviewgateway.cpp
}

# On Android we use the Android webkit in the demo app
#_______________________________________________________________________________

android {
SOURCES += \
    Gateway/4SDC2Android/platformwebbrowser.cpp \
    Gateway/4SDC2Android/WebBrowser.cpp \
    Gateway/Android2WebKit/android2webkit.cpp

HEADERS += \
    Gateway/4SDC2Android/platformwebbrowser.h \
    Gateway/Android2WebKit/android2webkit.h

OTHER_FILES += \
    android/AndroidManifest.xml \
    android/res/drawable-hdpi/icon.png \
    android/res/drawable-ldpi/icon.png \
    android/res/drawable-mdpi/icon.png \
    android/res/layout/html5layout.xml \
    android/src/dk/fs/fsdc/framework/FSDC2JSGateway.java \
    android/src/dk/fs/fsdc/framework/FSDCWebViewClient.java \
    android/src/dk/fs/fsdc/framework/WebkitActivity.java \
    android/src/dk/fs/fsdc/framework/WebkitLauncher.java

ANDROID_PACKAGE_SOURCE_DIR += \
    $$PWD/android
}

################################################################################
# Files for 4SDC stack
################################################################################
include("../Shared.pro/4SDC.pro")

################################################################################
# Files for PAL layer
################################################################################
include("../Shared.pro/PAL.pro")

################################################################################
# Files for FSYS library
################################################################################
include("../Shared.pro/FSYS.pro")



/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief QML file containing the main view of the Demo application
 *
 * This file contains the main view of the Demo application described in QML
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Controls 1.1

Window {
    id: window1
    visible: true
    width: 360
    height: 360



    Rectangle {
        id: rectangle1
        //x: 100
        //y: 15
        color: "#FFFFFF"
        anchors.fill: parent
        visible: !settingsWindow.visible

        MouseArea {
            id: mouseArea1
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.leftMargin: 0
            anchors.topMargin: 0
            anchors.fill: parent

            onClicked: {
                //logoRotator.start();
                cpp_state.launchBrowser();
                // Qt.quit();
            }

            Image {
                id: image4sLogo
                //width: (5*parent.width)/100
                width: parent.width/4
                anchors.rightMargin: 0
                //anchors.bottomMargin: (5*parent.width)/100
                //anchors.leftMargin: (5*parent.width)/100
                anchors.topMargin: 0
                anchors.top: parent.top
                anchors.right: parent.right
                //anchors.left: parent.left
                fillMode: Image.PreserveAspectFit
                source: "///gfx/gfx/4S-grafik-artwork-stor.png"
            }

            Image {
                id: image1
                //width: (5*parent.width)/100
                anchors.rightMargin: (5*parent.width)/100
                anchors.bottomMargin: (5*parent.width)/100
                anchors.leftMargin: (5*parent.width)/100
                anchors.topMargin: (5*parent.width)/100
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "///gfx/gfx/4SDC_artwork.png"

                RotationAnimation {
                    id: logoRotator
                    target: image1
                    property: "rotation"
                    easing.type: Easing.InOutQuad
                    duration: 1300
                    from: 360
                    to: 0
                }

    /*            Timer {
                    interval: 10000;
                    running: true;
                    repeat: true;
                    onTriggered: {

                        logoRotator.start();
                    }
                }  */
            }
        }
        Text {
            id: idTextHeader
            color: "#000000" //"#11e305"

            text: qsTr("Tap the screen to start")
            anchors.top: parent.top
            anchors.topMargin: 15
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Button {
            id: buttonExit

            text: qsTr("Exit")
            anchors.right: parent.right
            anchors.rightMargin: 8
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 8

            onClicked: {
                Qt.quit();
            }
        }

        Text {
            x: 8
    //        width: 228
    //        height: 18
            color: "#000000" //"#11e305"
            text: qsTr("Powered by 4SDC")
            //anchors.horizontalCenterOffset: 0
            anchors.top: idTextHeader.bottom
            anchors.topMargin: 22
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Button {
            id: button1
            y: 325
            text: qsTr("Configure")
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 8
            anchors.left: parent.left
            anchors.leftMargin: 8

            onClicked: {
                //rectangle1.visible = false;
                settingsWindow.visible = true;
            }
        }


    }
    SettingsDialog {
        id: settingsWindow
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent
        visible: false
    }
}



/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Implementation file for the AppController class
 *
 * Contains implementations for the AppController class
 *
 * /see AppController
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
#include "appcontroller.h"

#include "FSYS/msgqueue.h"
#include "FSYS/log.h"

#include <QtGlobal>
#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QTimer>

#include <mutex>

DECLARE_LOG_MODULE("Appcontroller")

FSYS::ModulePrivate::ModuleThreader AppController::moduleThreader;
AppController *AppController::me = NULL;

AppController::AppController(QObject *parent) :
  QObject(parent),
  app(NULL),
  engine(NULL)
{
  // Catch if we are being initialized after any of the static functions
  // has been called
  Q_ASSERT(me == NULL);

  // Store a copy of our self
  me = this;
}


AppController& AppController::get( void )
{
  // Protect this function
  static std::mutex protector;
  std::lock_guard<std::mutex> lock (protector);

  // If there is no "me" then create one
  if(!me) new AppController;

  // Return a reference to "me"
  return *me;
}

AppController::~AppController()
{
  // We assume we can only be deleted from one thread
  get();

  // Delete objects if they were allocated (note: delete (type*)0 is valid code)
  delete me->engine;
  delete me->app;

  // Zero out the pointers to the used types (to ensure there aren't versions
  // floating around that still uses these pointers
  me->app = NULL;
  me->engine = NULL;
}

void AppController::registerObjects(void)
{
  // We don't do anything in this function, it is only implemented so simple
  // programs don't have to overwrite it
}

int AppController::launchApp( int argc,
                              char *argv[],
                              QString urlToLoad )
{  
  return launchApp(argc, argv, QUrl(urlToLoad));
}

int AppController::launchApp( int argc,
                              char *argv[],
                              QUrl urlToLoad )
{
  // Protect this function
  static std::mutex protector;
  std::lock_guard<std::mutex> lock (protector);

  // Ensure we have a global me object;
  get();

  // If we don't have an app, then create one
  if(!me->app) me->app = new QApplication(argc, argv);   

  // If we don't have an engine then create one (must happen after the
  // QGuiApplication is created
  if(!me->engine) me->engine = new QQmlApplicationEngine;

  // Allow child classes to register objects that are needed by the initial
  // QML code.
  me->registerObjects();

  // Start the threading system
  moduleThreader.spawnAll();

  // Load the qml making the application
  me->engine->load( urlToLoad );

  // Run the application
  int loopResult = runEventLoop();

  // Stop the threading system
  moduleThreader.terminateAll();

  // Return the loop result
  return loopResult;
}

int AppController::runEventLoop( void )
{
  // Start a timer
  QTimer::singleShot(100, me, SLOT(loop4SEventLoop()));
  return me->app->exec();
}

void AppController::loop4SEventLoop( void )
{
  //INFO("void AppController::loop4SEventLoop( void )");

  // As long as there are message on the 4S queue, empty it
  while(!FSYS::MsgQueue::isEmpty())
  {
    // Run the 4S msg loop for 100 ms, or until it is empty
    FSYS::MsgQueue::emptyMsgQueue(100);

    // Run the Qt msg loop for 100 ms, or until it is empty
    me->app->processEvents(QEventLoop::AllEvents, 100);
  }

  // Introduce a 100 ms delay before we check the Qt message queue again
  QTimer::singleShot(100, me, SLOT(loop4SEventLoop()));
}

QQmlContext *AppController::qmlContext( void )
{
  // If there is an engine object, then retrieve the root context, otherwise
  // return NULL
  return (me->engine)?me->engine->rootContext():NULL;
}


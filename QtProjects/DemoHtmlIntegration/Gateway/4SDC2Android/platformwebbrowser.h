/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Header file for the PlatformWebBrowser class for Android
 *
 * /see PlatformWebBrowser
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef PLATFORMWEBBROWSER_H
#define PLATFORMWEBBROWSER_H

#include <QObject>
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>

/**
 * @brief This PlatformWebBrowser class is the Android specific browser abstraction
 *
 *
 */
class PlatformWebBrowser : public QObject
{
  Q_OBJECT

  /**
   * @brief Flags if the PAL::Android::setContext() has been called.
   */
  static bool contextRegistered;

  /**
   * @brief Function being called from Java when the JS on the web page requests a measurement
   *
   * @param env
   * @param thiz
   * @param reqType The request string (i.e. what kind of measurment is requested)
   * @param jsCallBackFunc The name of the function that should be called in js
   *                       when a result is ready.
   */
  static void requestMeasurement(JNIEnv *env,
                                 jobject thiz,
                                 jstring reqType,
                                 jstring jsCallBackFunc);

  /**
   * @brief Function being called from Java when the JS on the web page requests the copyright string
   *
   * @param env
   * @param thiz
   *
   * @return The copyright string for this version of the 4SDC module
   */
  static jstring copyright(JNIEnv *env,
                           jobject thiz);

  /**
   * @brief Function being called from Java when the JS on the web page requests the version string
   *
   * @param env
   * @param thiz
   *
   * @return The version string for this version of the 4SDC module
   */
  static jstring version(JNIEnv *env,
                         jobject thiz);

  // Function to terminate the qt app
  static void terminateQtApp(JNIEnv *env, jobject thiz);

public:
  PlatformWebBrowser(void);

  /**
   * @brief Function to create a browser window
   *
   * It is undefined behavior to call this while another browser window is
   * active.
   *
   * @param URL  The URL of the page that should be loaded
   */
  void createBrowserWindow(std::string const URL);

  /**
   * @brief Function will execute the jsString in the browser
   *
   * This function is called to execute some JS code in the active browser.  It
   * it is undefined behavior to call this function when there are no browser
   * open
   *
   * @param jsString The JS code that should be executed
   *
   * @return true If the JS code was send to the browser
   *
   * @return false If the JS code was not send to the browser
   */
  bool runJSInBrowser(std::string const jsString);



};

#endif // PLATFORMWEBBROWSER_H

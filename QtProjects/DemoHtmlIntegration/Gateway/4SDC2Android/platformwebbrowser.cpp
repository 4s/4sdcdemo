/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Implementation of the PlatformWebBrowser class for Android
 *
 * /see PlatformWebBrowser
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
#include "platformwebbrowser.h"
#include "PAL/android.h"

#include "Gateway/webbrowser.h"
#include "Gateway/jsongateway.h"
#include "FSYS/log.h"
#include <mutex>
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#include <QtGlobal>

// Enable program termination from this file
#include <QCoreApplication>

DECLARE_LOG_MODULE("PlatformWebBrowser")

/*
 * JNI Data type table
 *
 * Native Type     Java Language Type  Description        Type signature
 * unsigned char   jboolean            unsigned 8 bits    Z
 * signed char     jbyte               signed 8 bits      B
 * unsigned short  jchar               unsigned 16 bits   C
 * short           jshort              signed 16 bits     S
 * long            jint                signed 32 bits     I
 * long long       jlong               signed 64 bits     J
 * float           jfloat              32 bits            F
 * double          jdouble             64 bits            D
 * void                                                   V
*/

void PlatformWebBrowser::requestMeasurement(JNIEnv *env,
                                            jobject thiz,
                                            jstring reqType,
                                            jstring jsCallBackFunc)
{
  Q_UNUSED(thiz)

  Gateway::JSONGateway::requestMeasurement(env->GetStringUTFChars(reqType, 0),
                                  env->GetStringUTFChars(jsCallBackFunc, 0));
}

jstring PlatformWebBrowser::copyright(JNIEnv *env, jobject thiz)
{
  Q_UNUSED(thiz)

  std::string crString = Gateway::JSONGateway::copyright();
  return env->NewStringUTF(crString.c_str());
}

jstring PlatformWebBrowser::version(JNIEnv *env, jobject thiz)
{
  Q_UNUSED(thiz)

  std::string crString = Gateway::JSONGateway::version();
  return env->NewStringUTF(crString.c_str());
}


// Function that requests termination of the qt app
void PlatformWebBrowser::terminateQtApp(JNIEnv *env, jobject thiz)
{
  Q_UNUSED(thiz)
  Q_UNUSED(env)
  // Request main event loop to stop (which in turn will start program termination)
  QCoreApplication::exit();
}

bool PlatformWebBrowser::contextRegistered = false;

PlatformWebBrowser::PlatformWebBrowser(void)
{
  if (!contextRegistered) 
  {
    QAndroidJniObject c = QAndroidJniObject::callStaticObjectMethod(
                  "dk/fs/fsdc/framework/WebkitLauncher",
                  "getContext",
                  "()Landroid/content/Context;");
    PAL::Android::setContext(c.object<jobject>());

    contextRegistered = true;
  }

  // Register native/c++ function to handle call from Java
  static const JNINativeMethod methods[] = {{"staticRequestMeasurement",
                                             "(Ljava/lang/String;Ljava/lang/String;)V",
                                             reinterpret_cast<void *>(requestMeasurement)},
                                            {"staticCopyright",
                                             "()Ljava/lang/String;",
                                             reinterpret_cast<void *>(copyright)},
                                            {"staticVersion",
                                             "()Ljava/lang/String;",
                                             reinterpret_cast<void *>(version)},
                                           };


  QAndroidJniObject javaClass("dk/fs/fsdc/framework/FSDC2JSGateway");

  QAndroidJniEnvironment env;
  jclass objectClass = env->GetObjectClass(javaClass.object<jobject>());
  env->RegisterNatives(objectClass,
                       methods,
                       sizeof(methods) / sizeof(methods[0]));
  env->DeleteLocalRef(objectClass);

  static const JNINativeMethod otherMethods[] = {{"staticTerminateQtApp",
                                                  "()V",
                                                  reinterpret_cast<void *>(terminateQtApp)}
                                                };




  // Register our signal slot link in Qt, always use queued connections
 // connect(this, &PlatformWebBrowser::sendJSCode,
   //       this, &PlatformWebBrowser::receiveJSCode,
     //     Qt::QueuedConnection);
}

void PlatformWebBrowser::createBrowserWindow(std::string const URL)
{
  //DEBUG("Enter BrowserWindowLaunch");

  QString qurl(URL.c_str());
  QAndroidJniObject jurl = QAndroidJniObject::fromString(qurl);
  jint value = QAndroidJniObject::callStaticMethod<jint>("dk/fs/fsdc/framework/WebkitLauncher",
                                                         "staticLaunchWebKit",
                                                         "(Ljava/lang/String;)I",
                                                         jurl.object<jstring>());
  Q_UNUSED(value);



  //DEBUG("Leave BrowserWindowLaunch");
}


bool PlatformWebBrowser::runJSInBrowser(std::string const jsStrin)
{
  //DEBUG("Calling webpage with :" + jsString);
  //DEBUG(jsString.c_str());
  QString jsString(jsStrin.c_str());


  QAndroidJniObject jjs = QAndroidJniObject::fromString(jsString);

  /*jboolean value =*/ QAndroidJniObject::callStaticMethod<jboolean>(
          "dk/fs/fsdc/framework/WebkitActivity",
          "runJS",
          "(Ljava/lang/String;)Z",
          jjs.object<jstring>()
        );
    return true;
}


/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Header file for the PlatformJSONGateway class
 *
 * The JSON gateway, is the one that does the actuall communication with the
 * rest of the system in the link between the browser and 4SDC code.
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef PLATFORMJSONGATEWAY_H
#define PLATFORMJSONGATEWAY_H

#include "4SDC/core.h"
#include "Gateway/webbrowser.h"
#include "FSYS/msgreceiver.h"
#include "FSYS/msgsender.h"
#include <map>

namespace Gateway
{

class PlatformJSONGateway : public FSYS::MsgSender<PlatformJSONGateway>,
                            public FSYS::MsgReceiver<PlatformJSONGateway, fsdc::Core::DataAvailable>
{
private:
  /**
   * @brief Member that stores a pointer to the browser
   *
   * This member should not be accessed directly, always use the "getBrowser"
   * function to get the value of it.
   *
   * The browser pointer implements a callback function that will run JS in the
   * browser context.
   */
  Gateway::WebBrowser *browser = {nullptr};

  /**
   * @brief Function to read the browser member of this class
   *
   * @return The browser member (this might be the nullptr)
   */
  Gateway::WebBrowser *getBrowser(void);

  /**
   * @brief Map that keeps track of the which callback function to call
   *
   * Holds a map between the device ID's and the current callback function
   *
   * If an entry is the empty string, it means the app should not be listening
   * on that device ID.
   */
  std::map<uint16_t, std::string> callBackMap;

private:
  /**
   * @brief Function to map between human readable strings and type ids
   *
   * This function takes a human readable string like "BloodPressure" and
   * converts it to a corresponding ID - for the blood pressure case this id is
   * 0x1007
   *
   * @param typeString The human readable string
   *
   * @return The Communication infrastructure ID corresponding to the human
   *         readable string or 0 if no match
   */
  uint16_t idFromTypeString(std::string typeString);

public:
  /**
   * @brief Sets the browser that this class should interface with
   *
   * @param browser A pointer to the browser
   */
  void setBrowser(Gateway::WebBrowser *browser);

  /**
   * @brief Function to get a measurement into the system
   *
   * @param measurementType A human readable string that identifies the type
   *                        of measurement that should be read
   *
   * @param callBackFunction The JS function that should be called when new data
   *                         is available, or "" if you are no longer interested
   *                         in listening on a message.
   */
  void requestMeasurement(std::string measurementType,
                          std::string callBackFunction);

  /**
   * @brief Function that resets this module into its default state
   *
   * Part of the reset, is to unsubscribe from measurements that we listen for
   * and clearing of internal data structures
   */
  void reset( void );

  /**
   * @brief Receive function for data from the session layer
   *
   * When data is available from the session layer it will be sent with the
   * DataAvailable function.
   *
   * @param data The data object containing type and a JSON string
   */
  void receive( fsdc::Core::DataAvailable data );
};

}


#endif // PLATFORMJSONGATEWAY_H


/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Implementation file for the Gateway::JSONGateway class
 *
 * /see Gateway::JSONGateway
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "platformjsongateway.h"
#include "Gateway/jsongateway.h"
#include "FSYS/log.h"

#include <QObject>
#include <QThread>

DECLARE_LOG_MODULE("JSON Gateway");


Gateway::PlatformJSONGateway *Gateway::JSONGateway::getGateway(void)
{
  static Gateway::PlatformJSONGateway jsongateway;
  return &jsongateway;
}

void Gateway::JSONGateway::setBrowser(Gateway::WebBrowser *browser)
{
  getGateway()->setBrowser(browser);
}

std::string Gateway::JSONGateway::copyright(void)
{
  INFO("Copyright string requested from JSONGateway");

  return "4SDC Copyright (c) 4S 2014-2015 - All rights reserved, source code released under Apache 2.0";
}

std::string Gateway::JSONGateway::version(void)
{
  INFO("Version string requested from JSONGateway");

  return "1.0";
}

void Gateway::JSONGateway::requestMeasurement(std::string measurementType, std::string callBackFunction)
{
  //INFO("C++ requestMeasurement called");
  getGateway()->requestMeasurement(measurementType, callBackFunction);
}

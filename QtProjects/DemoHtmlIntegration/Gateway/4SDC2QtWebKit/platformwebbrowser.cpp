/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Implementation file for the PlatformWebBrowser class for Qt Webkit
 *
 * The Qt Webkit is the prefered way to open a browser, we only use something
 * else on platforms that don't support Qt Webkit (like Android)
 *
 * /see PlatformWebBrowser
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */


#include "appcontroller.h"
#include "FSYS/log.h"
#include "platformwebbrowser.h"
#include "webviewgateway.h"

#include <QQmlContext>
#include <QDir>
#include <QObject>
#include <QQuickView>
#include <QQuickItem>
#include <QWebFrame>
#include <QWebView>

#include <typeinfo>

DECLARE_LOG_MODULE("QtBrowser")

// Make sure the resource file is included in the lib
void includeResources( void )
{
  Q_INIT_RESOURCE(browser);
}

PlatformWebBrowser::PlatformWebBrowser(QObject *parent)
  : QObject(parent)
  , view(NULL)
{
  // Create new WebViewGateway object
  webViewGateway = new WebViewGateway;
}

void PlatformWebBrowser::javaScriptWindowObjectCleared(void)
{
  // Restore the fsdc object
  frame->addToJavaScriptWindowObject("fsdc", webViewGateway);
}

void PlatformWebBrowser::createBrowserWindow(QString url)
{
  // Rememeber the URL
  this->url = url;
  urlChanged();

  INFO("Create Browser Window");

  // If we don't have a view, then create one
  if(NULL == view)
  {
    view = new QWebView;
  }

  // Load the URL
  view->load(QUrl(url));

  // Make sure the page is shown
  view->show();

  // Get the details about the page (might be redundant the second time we get here)
  myWebPage = view->page();
  frame = myWebPage->mainFrame();
  frame->addToJavaScriptWindowObject("fsdc", webViewGateway);

  // Connect the relevant signals, if they are not already connected
  connect(this, &PlatformWebBrowser::evaluateJavaScript,
          frame, &QWebFrame::evaluateJavaScript,
          Qt::QueuedConnection /*| Qt::UniqueConnection*/);

  connect(frame, &QWebFrame::javaScriptWindowObjectCleared,
          this, &PlatformWebBrowser::javaScriptWindowObjectCleared,
          Qt::QueuedConnection /*| Qt::UniqueConnection*/);

}

bool PlatformWebBrowser::runJSInBrowser(std::string const jsString)
{
  INFO("Calling browser with:" + jsString);
  emit evaluateJavaScript(QString::fromStdString(jsString));

  return true;
}

void PlatformWebBrowser::launchBrowser( void )
{

}

/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Header file for the Gateway::WebBrowser interface
 *
 * /see Gateway::WebBrowser
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef WEBBROWSER_H
#define WEBBROWSER_H

#include <string>

class PlatformWebBrowser;

namespace Gateway
{
  /**
   * @brief Class to control a browser
   *
   * This class is used to launch a browser that will let the loaded
   * pages communicate with the 4SDC interface.
   *
   * It is the responsibility of the caller to ensure the safety of loading
   * pages with this interface.
   *
   * @code
   *   #include <Interface\webbrowser.h>
   *
   *   class myBrowserCallBackClass : protected Gateway::WebBrowser::Callback
   *   {
   *     void LoadFail(Gateway::WebBrowser browser, std::string URL)
   *     {
   *       // Handle case when browser failed to load the URL
   *     }
   *   } myBrowserCallBack;
   *
   *   Gateway::WebBrowser myBrowserSession(myBrowserCallBack);
   *
   *   // Function to launch browser
   *   void launchBrowser()
   *   {
   *     myBrowserSession.launch("http://4s-online.dk/4SDC/");
   *   }
   *
   *   // Function to close browser
   *   void closeBrowser()
   *   {
   *     myBrowserSession.Terminate();
   *   }
   * @endcode
   *
   * It is platform dependent how the browser launches.
   */
  class WebBrowser
  {
  private:

    /**
     * @brief Real browser object
     *
     * The realBrowser member is a convinience object that can be used by the
     * platform dependant browser code.
     */
    PlatformWebBrowser *realBrowser;
  public:
    /**
     * @brief Class used by the browser to communicate back to the caller.
     *
     * If you want to be informed about events that happens in the browser
     * then you must implement this class and give an instance of it to
     * the WebBrowser constructor.
     *
     * Using this class is optional.
     *
     * @warning It is platform dependant in which thread context the functions
     *       in this interface are called in, and there might be multiple
     *       thread contexts used, so code implementing this must be re-entrant.
     *
     * @note You can implement any number of the functions in this interface,
     *       you specifically don't need to implement all of them to use the
     *       interface.
     */
    class Callback
    {
      /**
       * @brief Function is called if loading of a page fails
       *
       * On platforms that support it, this function will be called if loading
       * of the given URL fails.
       *
       * @param browserInstance The browser instance where this page was loaded
       *                        from.
       *
       * @param URL The URL of the page that failed to load.
       *
       * @sa WebBrowser::platformSupportsSendingLoadFails
       */
      virtual void LoadFail(WebBrowser browserInstance, std::string const URL);
    };

    /**
     * @brief Constructor
     *
     * The default constructor doesn't take any parameters
     */
    WebBrowser( void );
    ~WebBrowser();

    /**
     * @brief Function to launch a new window with the given URL
     *
     * @param URL The url that should be loaded
     */
    void launch(std::string const URL);

    /**
     * @brief Function to excute JS on a loaded web page
     *
     * This function is thread safe.
     *
     * @param jsString The JS string that should be executed
     *
     * @return true if the js was executed, false if not
     */
    bool runJSInBrowser(std::string const jsString);

    /**
     * @brief Retrieve platform dependent information about the loading behavior
     *
     * This function can be called to intereogate about the behavior of the
     * loading function.
     *
     * @return true if the platform supports sending LoadFail callbacks, false
     *         otherwise
     *
     * @sa launch Callback::LoadFail
     */
    bool platformSupportsSendingLoadFails( void );

    /**
     * @brief Terminate the browser
     *
     * On platforms that support it, calling this function will terminate the
     * browser window.
     *
     * You can call platformSupportsTerminateFunction() to investigate if a
     * given platform supports the terminate function or not.  In case the
     * function is unsupported, it is guaranteed, that calling ther terminate
     * function won't have any ill effects.
     *
     * @sa platformSupportsTerminateFunction( void );
     */
    void terminate();

    /**
     * @brief Retrieve platform dependent information about the support of the terminate function
     *
     * Calling this funciton will tell you if the platform supports the
     * terminate function.  Calling the terminate function on unsupported
     * platforms will be ignored.
     *
     * @return true if the platform supports the terminate function, false if not
     */
    bool platformSupportsTerminateFunction( void );

  };


}

#endif // WEBBROWSER_H

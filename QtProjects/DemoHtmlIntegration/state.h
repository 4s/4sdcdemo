/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Header file for class that is accessible from QML
 *
 * /see State
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef STATE_H
#define STATE_H


#include "state.h"
#include <QObject>

namespace Gateway {
  class WebBrowser;
  class JSONGateway;
}

class QQmlContext;

/**
 * @brief Class holding the interface between QML and C++
 *
 * This class can be accessed by the Qt QML GUI, and is used to keep the QML and
 * C++ in sync, as well as translating GUI events to actions in code.
 *
 */
class State : public QObject
{
  Q_OBJECT

  /**
   * @brief Pointer to browser object
   */
  Gateway::WebBrowser *pBrowser;
  Gateway::JSONGateway *pJSONGateway;

  QString url;

public:
  explicit State(QObject *parent = 0);

  ~State( void );
  /**
   * @brief Register this class so it is accessible from QML
   *
   * Call this function with a QML context to register this class in that
   * context (so it can be accessed from that context).
   *
   * @param context The context where this class should be registered
   */
  void registerInContext( QQmlContext *pContext );

  /**
   * @brief Function to launch a web browser
   *
   * Calling this function will launch the webbrowser and attempt to load the
   * page the url property points to.
   */
  Q_INVOKABLE void launchBrowser( void );

  Q_PROPERTY( QString url MEMBER url NOTIFY urlChanged )

signals:
  void urlChanged(void);

public slots:

};

#endif // STATE_H




package dk.fs.fsdc.framework;

/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Part of the Gateway interface between C++ and JS
 *
 * Contains the Java class that is exposed to the JS running on the web page
 * being displayed in the browser.
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

import android.webkit.JavascriptInterface;
import android.util.Log;

public class FSDC2JSGateway {
  private static final String TAG = "FSDC2JSGateway";

  @JavascriptInterface
  public String version(){
    Log.d(TAG, "Version requested");
    return staticVersion();
  };

  @JavascriptInterface
  public String copyright() {
    Log.d(TAG, "Copyright statement requested");
    return staticCopyright();
  };

  @JavascriptInterface
  public void requestMeasurement(String measurementType, String jsCallBackFunc)
  {
    Log.d(TAG, "Measurement of type: " + measurementType +
    " is requested, callback to: " + jsCallBackFunc);

    if(null == measurementType)
    {
      Log.e(TAG, "First parameter to fsdc.requestMeasurement(...) is not a valid string");
      Log.d(TAG, "Call to request measurement ignored due to previous error");
      return;
    }

    if(null == jsCallBackFunc)
    {
      jsCallBackFunc = "";
      Log.d(TAG, "jsCallBackFunc was null, converted to the empty string");
    }

    // Calling the static c++ function to handle this
    staticRequestMeasurement(measurementType, jsCallBackFunc);
  }

  public static native String staticCopyright();
  public static native String staticVersion();
  public static native void staticRequestMeasurement(String measurementType, String jsCallBackFunc);
}


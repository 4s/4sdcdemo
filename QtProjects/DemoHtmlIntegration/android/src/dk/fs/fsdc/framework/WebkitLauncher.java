package dk.fs.fsdc.framework;

/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief %Android Java webkit launcher.
 *
 * This class has the responsibility for launching the webkit activity so a
 * browser page can be displayed to the user.
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import org.qtproject.qt5.android.bindings.QtActivity;


public class WebkitLauncher extends QtActivity {

    private static final String TAG = "WebkitLauncher";

    private static WebkitLauncher activeActivity = null;

    // This is the starting point of the entire application!
    // Notice that the Qt debug-server will not start until this
    // method returns, so it is not possible to make breakpoints in
    // code called from this method.
    @Override public void onCreate(Bundle savedInstanceState)
    {
        // The superclass-method will initialize the C++ libraries
        // including Qt and the JNI_OnLoad in PAL/Android/android.cpp
        super.onCreate(savedInstanceState);
        activeActivity = this;
        // Done initializing. Now the main() method is up next (probably
        // on a new thread)
    }

    public void launchWebKit() {

      Log.d(TAG, "We are in Android " );

    }

    public static Context getContext() {
        return activeActivity;
    }

    static public int staticLaunchWebKit(String url) {
      Log.d(TAG, "staticLaunchWebKit(" + url + ")" );

      if(null != activeActivity)
      {
        //define a new Intent for the second Activity
        Intent intent = new Intent(activeActivity, dk.fs.fsdc.framework.WebkitActivity.class);
        intent.setData(Uri.parse(url));

        Log.d(TAG, "Starting browser activity by sending intent");

        //start the second Activity
        activeActivity.startActivity(intent);
        return 0;
      }
      else
      {
        Log.d(TAG, "Main activity hasn't started yet");
      }


      return -1;
    }

    // JNI function to terminate the QtApp
    //public static native void staticTerminateQtApp();

}

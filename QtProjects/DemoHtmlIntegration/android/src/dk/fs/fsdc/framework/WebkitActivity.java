package dk.fs.fsdc.framework;

/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief %Android Java activity for the webkit browser.
 *
 * Contains the Java activity that is the (%Android) webkit browser.
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.JavascriptInterface;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.view.View.OnClickListener;
import android.view.View;
import java.lang.String;

public class WebkitActivity extends Activity {

    private static final String TAG = "BrowserActivity";
    static WebView myBrowser;
    static String urlLoaded;
    EditText myText;

    static boolean runJS(final String jsString)
    {
      // Make sure the browser isn't destroyed under our feets
      final WebView browserCopy = myBrowser;
      if(null != browserCopy)
      {
        // Make sure we are in the right thread when we call the JS
        browserCopy.post(new Runnable(){

          @Override
          public void run() {
            browserCopy.loadUrl("javascript:"+jsString);
          }
        });
        return true;
      }
      return false;
    }


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get the intent that triggered us
        Intent intent = getIntent();

        // Find the uri that we should launch with
        Uri uri = intent.getData();

        // Stor the URL
        urlLoaded = uri.toString();

        Log.d(TAG, "Creating the BrowserActivity("+intent.getData()+")");

        // Link this activity with the resource describing the view
        this.setContentView(R.layout.html5layout);

        // Find the browser component
        myBrowser = (WebView)findViewById(R.id.mybrowser);

        // Set the webview to our own webview
        myBrowser.setWebViewClient(new FSDCWebViewClient());

        // Enable JS on the page
        myBrowser.getSettings().setJavaScriptEnabled(true);

        // Link browser with the 4SDC gateway java object
        myBrowser.addJavascriptInterface(new FSDC2JSGateway(), "fsdc");

        // Load the uri
        myBrowser.loadUrl(urlLoaded);

    }

    @Override
    public void onDestroy()
    {
      super.onDestroy();

      myBrowser = null;

      // Call terminating function
//      staticTerminateQtApp();
    }

    public void reloadButtonPress(View view)
    {
      myBrowser.clearCache(true);
      myBrowser.loadUrl(urlLoaded);
    }

    // JNI function to terminate the QtApp
    public static native void staticTerminateQtApp();
}

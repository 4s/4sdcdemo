#ifndef TST_MSGSINGLETHREAD_H
#define TST_MSGSINGLETHREAD_H

#include <QString>
#include <QtTest>
#include <QObject>

class tst_msgSingleThread : public QObject
{
  Q_OBJECT

public:
  tst_msgSingleThread();
  ~tst_msgSingleThread();

private Q_SLOTS:
  void testQueueHasHandle( void );

  void testBroadcast( void );

  void testSend( void );

  void testOrder( void );
};

#endif // TST_MSGSINGLETHREAD_H

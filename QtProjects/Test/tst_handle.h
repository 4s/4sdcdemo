#ifndef TST_HANDLE_H
#define TST_HANDLE_H

#include <QString>
#include <QtTest>
#include <QObject>

class tst_handle : public QObject
{
  Q_OBJECT
public:
  explicit tst_handle(QObject *parent = 0);
  ~tst_handle();

signals:

public slots:

private Q_SLOTS:
  void testNoHandlesAreEqualInOneThread( void );
  void testNoHandlesAreNull( void );
  void testAssignment( void );
  void testCompare( void );

};

#endif // TST_HANDLE_H

#include "tst_msgmultithread.h"

#include "FSYS/handle.h"
#include "FSYS/msgqueue.h"
#include "FSYS/msgreceiver.h"
#include "FSYS/msgsender.h"
#include "FSYS/basemsg.h"

#include <thread>

// We don't want to polute...
namespace
{
// Dummy message that can carry an integer
class TestMsg1 : public FSYS::BaseMsg
{
public:
  int i;
  TestMsg1():i(0){}
};

// A different dummy message
class TestMsg2 : public FSYS::BaseMsg
{
public:
  int i;
  TestMsg2():i(0){}
};

class TestTerminate : public FSYS::BaseMsg
{

};

class HandleSender : public FSYS::BaseMsg
{
public:
  FSYS::Handle handle;
};


// Class that loops a message back to the sender when received
template<class T> class TestLoopBack : public FSYS::MsgReceiver<TestLoopBack<T>, T>, public FSYS::MsgSender<TestLoopBack<T> >
{
public:
  T respondMsg;
  T receivedMsg;

  void receive(T &msg){FSYS::MsgSender<TestLoopBack<T> >::respond(respondMsg, msg);receivedMsg=msg;}
};

template<class T> class TestSendReceive : public FSYS::MsgReceiver<TestSendReceive<T>, T>, public FSYS::MsgSender<TestSendReceive<T> >
{
public:
  T receivedMsg;

  void receive(T &msg){receivedMsg=msg;}
};

}

tst_msgMultiThread::tst_msgMultiThread()
{

}

tst_msgMultiThread::~tst_msgMultiThread()
{

}

// The second thread
void secondThread()
{
  TestLoopBack<TestMsg2> loopBack;

  class Terminator : public FSYS::MsgReceiver<Terminator, TestTerminate>,
                     public FSYS::MsgSender<Terminator>
  {
    bool terminate;
  public:
    void run( void )
    {
      terminate = false;

      while(false == terminate)
      {
        FSYS::MsgQueue::waitUntilQueueHasData();
        FSYS::MsgQueue::emptyMsgQueue();
      }
    }

    void receive(TestTerminate &)
    {
      terminate = true;
    }
  } terminator;

  HandleSender handleSender;

  handleSender.handle = FSYS::MsgQueue::getHandle();

  // Broadcast our handle
  terminator.broadcast(handleSender);

  terminator.run();
}

void tst_msgMultiThread::twoThreads( void )
{
  // We need an empty queue (a known state) before starting this test
  QVERIFY2(FSYS::MsgQueue::isEmpty(), "Queue not empty at start of test");

  // Generic message sender
  TestSendReceive<FSYS::BaseMsg> sender;

  TestSendReceive<HandleSender> handleReceiver;

  // Spawn worker thread
  std::thread second(secondThread);

  // Wait for the child thread to start
  FSYS::MsgQueue::waitUntilQueueHasData();

  // Read the data from the queue
  FSYS::MsgQueue::emptyMsgQueue();

  // Check that the second thread has a different queue handle than us
  QVERIFY2(FSYS::MsgQueue::getHandle() != handleReceiver.receivedMsg.handle,
           "Expected different queue handles from different threads");


  // Terminate second thread
  {
    // Send terminate event
    TestTerminate terminate;
    sender.broadcast(terminate);

    // Wait for second thread to terminate
    second.join();
  }



}

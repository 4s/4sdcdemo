#ifndef MODULEDECLARATION_H
#define MODULEDECLARATION_H


#include "FSYS/moduledeclare.h"

#include <map>

enum class State
{
  INITIAL,
  READY,
  GROUP_READY,
  SYSTEM_READY,
  GOING_DOWN,
  DESTROYED
};

extern std::map<State, int> stateList;
extern bool stateSwitchHappy;

class ModuleA : public FSYS::Module
{
public:
  // Place to store the state of the object
 State state = {State::INITIAL};

  void switchState(State newState)
  {
    // If the state switcher got unhappy, then it should remain unhappy
    if(!stateSwitchHappy) return;

    // Accept the legal state switches
    if((State::INITIAL == state && State::READY == newState)
    || (State::READY == state && State::GROUP_READY == newState)
    || (State::GROUP_READY == state && State::SYSTEM_READY == newState)
    || (State::SYSTEM_READY == state && State::GOING_DOWN == newState)
    || (State::GOING_DOWN == state && State::DESTROYED == newState))
    {
      // We are leaving the old state
      stateList[state]--;
      state = newState;
      // We are now in the new state
      stateList[state]++;
    }
    else
    {
      stateSwitchHappy = false;
    }

  }

  // Each callback maps to a state switch
  void msgReady( void ) override
  {
    switchState(State::READY);
  }

  void msgGroupReady( void ) override
  {
    switchState(State::GROUP_READY);
  }

  void msgSystemReady( void ) override
  {
    switchState(State::SYSTEM_READY);
  }

  void msgGoingDown( void ) override
  {
    switchState(State::GOING_DOWN);
  }

  void msgDestroy( void ) override
  {
    switchState(State::DESTROYED);
  }
};

class ModuleB : public FSYS::Module
{

};


#endif // MODULEDECLARATION_H


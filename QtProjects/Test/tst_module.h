#ifndef TST_MODULE_H
#define TST_MODULE_H

#include <QObject>
#include <QtTest>

class tst_module : public QObject
{
  Q_OBJECT
public:
  explicit tst_module(QObject *parent = 0);
  ~tst_module();

signals:

public slots:

private Q_SLOTS:
  // Called before each test is run
  void init( void );
  // Called after each test is run
  void cleanup( void );

  void checkGroupInThread( void );
  void checkRunnerInit( void );
  void checkRunnerMsgLoop( void );
  void checkThreadSystem( void );
};




#endif // TST_MODULE_H

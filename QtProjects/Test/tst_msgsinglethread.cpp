#include "tst_msgsinglethread.h"

#include "FSYS/handle.h"
#include "FSYS/msgqueue.h"
#include "FSYS/msgreceiver.h"
#include "FSYS/msgsender.h"
#include "FSYS/basemsg.h"

#include <queue>

// We don't want to polute...
namespace
{
// Dummy message that can carry an integer
class TestMsg1 : public FSYS::BaseMsg
{
public:
  int i;
  TestMsg1():i(0){}
};

// A different dummy message
class TestMsg2 : public FSYS::BaseMsg
{
public:
  int i;
  TestMsg2():i(0){}
};


// Class that loops a message back to the sender when received
template<class T> class TestLoopBack : public FSYS::MsgReceiver<TestLoopBack<T>, T>, public FSYS::MsgSender<TestLoopBack<T> >
{
public:
  T respondMsg;
  T receivedMsg;

  void receive(T &msg){FSYS::MsgSender<TestLoopBack<T> >::respond(respondMsg, msg);receivedMsg=msg;}
};

template<class T> class TestSendReceive : public FSYS::MsgReceiver<TestSendReceive<T>, T>, public FSYS::MsgSender<TestSendReceive<T> >
{
public:
  T receivedMsg;

  void receive(T &msg){receivedMsg=msg;}
};

}

tst_msgSingleThread::tst_msgSingleThread()
{

}

tst_msgSingleThread::~tst_msgSingleThread()
{

}

void tst_msgSingleThread::testQueueHasHandle( void )
{
  FSYS::Handle handle = FSYS::MsgQueue::getHandle();

  QVERIFY2(handle != FSYS::Handle::null(), "Queue handle, shouldn't be null handle");
  QVERIFY2(handle != FSYS::Handle::broadCastHandle(), "Queue handle, shouldn't be the broadcast handle");

}

void tst_msgSingleThread::testBroadcast( void )
{
  TestSendReceive<TestMsg1> receiver1;     // Object that will receive a message
  TestSendReceive<TestMsg2> receiver2;     // Object that will receive a message
  TestSendReceive<TestMsg1> receiver3;     // Object that will receive a message
  TestSendReceive<FSYS::BaseMsg> sender;   // Object that can send a message
  TestMsg1 msg1;                           // Message that can be send
  TestMsg2 msg2;

  // Veryfy all receivers are initialised to zero
  QVERIFY2(receiver1.receivedMsg.i == 0, "Receiver not initialized");
  QVERIFY2(receiver2.receivedMsg.i == 0, "Receiver not initialized");
  QVERIFY2(receiver3.receivedMsg.i == 0, "Receiver not initialized");

  msg1.i = 5;                  // Random number that should be sent
  msg2.i = 7;                  // A different number that should be sent

  // Send a 5
  sender.broadcast(msg1);

  // Check nothing is received until message loop is emptied
  QVERIFY2(receiver1.receivedMsg.i == 0, "Receiver not initialized");
  QVERIFY2(receiver2.receivedMsg.i == 0, "Receiver not initialized");
  QVERIFY2(receiver3.receivedMsg.i == 0, "Receiver not initialized");

  // Loop the message loop until empty
  FSYS::MsgQueue::emptyMsgQueue();

  // Veryfy all receivers received a 5
  QVERIFY2(receiver1.receivedMsg.i == 5, "Message broadcast not received by listener");
  QVERIFY2(receiver2.receivedMsg.i == 0, "Message broadcast received by someone who shouldn't get it");
  QVERIFY2(receiver3.receivedMsg.i == 5, "Message broadcast not received by listener");

  sender.broadcast(msg2);

  // Loop the message loop until empty
  FSYS::MsgQueue::emptyMsgQueue();

  // Veryfy that 2'nd receiver got the msg2
  QVERIFY2(receiver1.receivedMsg.i == 5, "Message broadcast received by someone who shouldn't get it");
  QVERIFY2(receiver2.receivedMsg.i == 7, "Message broadcast not received by listener");
  QVERIFY2(receiver3.receivedMsg.i == 5, "Message broadcast received by someone who shouldn't get it");
}

void tst_msgSingleThread::testSend( void )
{
  TestSendReceive<TestMsg1> sr1; // Object that will receive and send messages
  TestSendReceive<TestMsg2> sr2; // Object that will receive and send messages
  TestSendReceive<TestMsg1> sr3; // Object that will receive and send messages

  TestMsg1 msg1;                           // Message that can be send
  TestMsg2 msg2;

  // Initialise the messages so we can see which one (if any) has been recieved
  msg1.i = -1;
  msg2.i = 117;

  // Check nothing is received until message loop is emptied
  QVERIFY2(sr1.receivedMsg.i == 0, "Receiver not initialized");
  QVERIFY2(sr2.receivedMsg.i == 0, "Receiver not initialized");
  QVERIFY2(sr3.receivedMsg.i == 0, "Receiver not initialized");

  // Send message to receiver1
  sr3.send(msg1, sr1);

  // Loop the message loop until empty
  FSYS::MsgQueue::emptyMsgQueue();

  // Verify response
  QVERIFY2(sr1.receivedMsg.i == -1, "Receiver didn't get the message");
  QVERIFY2(sr2.receivedMsg.i == 0, "Receiver not initialized");
  QVERIFY2(sr3.receivedMsg.i == 0, "Receiver not initialized");

  // Let sr1 send a message back to sr3
  {
    TestMsg1 msg;
    msg.i = 119;
    sr1.respond(msg, sr1.receivedMsg);
  }

  // Loop the message loop until empty
  FSYS::MsgQueue::emptyMsgQueue();

  // Verify response
  QVERIFY2(sr1.receivedMsg.i == -1, "Wrong receiver got the message");
  QVERIFY2(sr2.receivedMsg.i == 0, "Wrong receiver got the message");
  QVERIFY2(sr3.receivedMsg.i == 119, "Receiver didn't get the message");

  // Check a message of the wrong type won't be received
  // Let sr2 listens for TestMsg2 type of message
  {
    TestMsg1 msg;
    msg.i = 123;
    sr1.send(msg, sr2);
  }

  // Loop the message loop until empty
  FSYS::MsgQueue::emptyMsgQueue();

  // Verify response
  QVERIFY2(sr1.receivedMsg.i == -1, "Wrong receiver got the message");
  QVERIFY2(sr2.receivedMsg.i == 0, "Wrong receiver got the message");
  QVERIFY2(sr3.receivedMsg.i == 119, "Wrong receiver got the message");

  // Check that more than one message can be on the queue, and still it will
  // find the right destination
  {
    TestMsg2 msg;
    msg.i = 2;
    sr1.send(msg, sr1);
    sr1.send(msg, sr2);
    sr1.send(msg, sr3);
  }

  {
    TestMsg1 msg;
    msg.i = 1;
    sr1.send(msg, sr1);
  }

  {
    TestMsg1 msg;
    msg.i = 3;
    sr1.send(msg, sr3);
  }

  // Loop the message loop until empty
  FSYS::MsgQueue::emptyMsgQueue();

  // Verify response
  QVERIFY2(sr1.receivedMsg.i == 1, "Receiver didn't get the message");
  QVERIFY2(sr2.receivedMsg.i == 2, "Receiver didn't get the message");
  QVERIFY2(sr3.receivedMsg.i == 3, "Receiver didn't get the message");
}

// Test that the order of messages sent to a module is correct
void tst_msgSingleThread::testOrder( void )
{
  class TestOrder : public FSYS::MsgReceiver<TestOrder, TestMsg1>
  {
  public:
    std::list<int> received;

    void receive(TestMsg1 &msg){received.push_back(msg.i);}
  } tobj;

  std::array<int, 10> inData = { 2, 4, 2, 67, 43, 2, 5, 66, 1, 0 };

  TestSendReceive<FSYS::BaseMsg> sender;

  for(int i : inData)
  {
    TestMsg1 msg;
    msg.i = i;
    sender.broadcast(msg);
  }

  // Loop the message loop until empty
  FSYS::MsgQueue::emptyMsgQueue();

  for(int i : inData)
  {
    QVERIFY2(i == tobj.received.front(), "UnexpectedValue received");
    tobj.received.pop_front();
  }

}

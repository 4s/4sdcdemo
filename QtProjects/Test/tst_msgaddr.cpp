#include "tst_msgaddr.h"

#include "FSYS/msgaddr.h"
#include "FSYS/basemsg.h"
#include "FSYS/msgqueue.h"
#include "FSYS/msgreceiver.h"
#include "FSYS/msgsender.h"

namespace
{
class MsgT1 : public FSYS::BaseMsg
{
};

class MsgT2 : public FSYS::BaseMsg
{
};


template<class T> class TestSendReceive : public FSYS::MsgReceiver<TestSendReceive<T>, T>, public FSYS::MsgSender<TestSendReceive<T> >
{
public:
  T receivedMsg;

  void receive(T &msg){receivedMsg=msg;}
};

}

tst_msgaddr::tst_msgaddr( void )
{

}

tst_msgaddr::~tst_msgaddr( void )
{

}

void tst_msgaddr::compare( void )
{
  TestSendReceive<MsgT1> obj1;
  TestSendReceive<MsgT1> obj2;

  // Send a message from object 1
  {
    MsgT1 msg1;
    obj1.broadcast(msg1);
  }

  // Loop the message loop until empty
  FSYS::MsgQueue::emptyMsgQueue();

  // Save the recevied message
  auto receive1 = obj1.receivedMsg;

  // Send another message from object 1
  {
    MsgT1 msg1;
    obj1.broadcast(msg1);
  }

  // Loop the message loop until empty
  FSYS::MsgQueue::emptyMsgQueue();

  // Save the received message
  auto receive2 = obj1.receivedMsg;

  // Send a third message from object 2
  {
    MsgT1 msg1;
    obj2.broadcast(msg1);
  }

  // Loop the message loop until empty
  FSYS::MsgQueue::emptyMsgQueue();

  // Save the received message
  auto receive3 = obj1.receivedMsg;

  // Check the two objects from the same sender are same,
  // and different from different senders
  QVERIFY2(receive1.getOriginAddr() == receive2.getOriginAddr(),
           "Two messages from the same sender should have same origin addr");
  QVERIFY2(receive2.getOriginAddr() == receive1.getOriginAddr(),
           "Two messages from the same sender should have same origin addr");
  QVERIFY2(receive1.getOriginAddr() != receive3.getOriginAddr(),
           "Two messages from the different senders should not have same origin addr");
  QVERIFY2(receive3.getOriginAddr() != receive1.getOriginAddr(),
           "Two messages from the different senders should not have same origin addr");
}

#include "tst_handle.h"

#include "FSYS/handle.h"

tst_handle::tst_handle(QObject *parent) : QObject(parent)
{

}

tst_handle::~tst_handle()
{

}

void tst_handle::testNoHandlesAreEqualInOneThread( void )
{
  FSYS::Handle h1, h2;

  QVERIFY2(h1 != h2, "Two handles should only be equal, if they are set equal");
}

void tst_handle::testNoHandlesAreNull( void )
{
  FSYS::Handle h1;

  QVERIFY2(h1 != FSYS::Handle::null(), "Handle should never be born null");
}

void tst_handle::testAssignment( void )
{
  FSYS::Handle h1, h2;

  QVERIFY2(h1 != h2, "Non equal handles should not compare equal");

  h1 = h2;

  QVERIFY2(h1 == h2, "Equal handles should compare equal");

  QVERIFY2(h1 != FSYS::Handle::null(), "Only null handle should be null");

  h1 = FSYS::Handle::null();

  QVERIFY2(h1 == FSYS::Handle::null(), "Null handle should be null");
}

void tst_handle::testCompare( void )
{
  FSYS::Handle h1, h2;

  QVERIFY2(h1 != h2, "Different handles should not compare equal");
  QVERIFY2(h2 != h1, "Different handles should not compare equal");
  QVERIFY2((h1 < h2 && h2 > h1) || (h1 > h2 && h2 < h1), "Order should not matter for comparison");
  QVERIFY2(h1 == h1, "Handle should compare equal to self");
  QVERIFY2(!(h1 > h1), "Handle can't' be bigger than self");
  QVERIFY2(!(h1 < h1), "Handle can't be smaller than self");
}

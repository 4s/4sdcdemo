#-------------------------------------------------
#
# Project created by QtCreator 2015-02-11T15:38:23
#
#-------------------------------------------------

# Turn on C++11 support
QMAKE_CXXFLAGS = -std=c++11
CONFIG += c++11

# Turn on thread support
LIBS += -pthread

QT       += testlib

QT       -= gui

TARGET = tst_maintest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += $$PWD/../../Interface

SOURCES += tst_maintest.cpp \
    tst_handle.cpp \
    tst_msgsinglethread.cpp \
    tst_msgmultithread.cpp \
    tst_msgaddr.cpp \
    tst_module.cpp

HEADERS += \
    tst_handle.h \
    tst_msgsinglethread.h \
    tst_msgmultithread.h \
    tst_msgaddr.h \
    tst_module.h \
    moduleconfig.h \
    moduledeclaration.h

DEFINES += SRCDIR=\\\"$$PWD/\\\"

################################################################################
# Files for 4SDC stack
################################################################################
include("../Shared.pro/4SDC.pro")

################################################################################
# Files for PAL layer
################################################################################
include("../Shared.pro/PAL.pro")

################################################################################
# Files for FSYS library
################################################################################
include("../Shared.pro/FSYS.pro")


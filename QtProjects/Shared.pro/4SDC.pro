################################################################################
#                                                                              #
#   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)                   #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
#                                                                              #
################################################################################
#                                                                              #
#   Contributer list:                                                          #
#                                                                              #
#     Jacob Andersen, The Alexandra Institute <jacob.andersen@alexandra.dk>    #
#     Mike Kristoffersen, The Alexandra Institute <4sdc@mikek.dk>              #
#                                                                              #
################################################################################

################################################################################
# Files for 4SDC stack
################################################################################

#-------------------------------------------------------------------------------
# Public interface
#-------------------------------------------------------------------------------
HEADERS += \
    ../../Interface/4SDC/core.h \
    ../../Interface/4SDC/simpledemowrapper.h

#-------------------------------------------------------------------------------
# Private implementation
#-------------------------------------------------------------------------------
HEADERS += \
    ../../Implementation/4SDC/InitialDemoLib/initialdemolib.h

SOURCES += \
    ../../Implementation/4SDC/InitialDemoLib/initialdemolib.cpp \
    ../../Implementation/4SDC/core/core.cpp

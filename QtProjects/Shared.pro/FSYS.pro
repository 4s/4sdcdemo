################################################################################
#                                                                              #
#   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)                   #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
#                                                                              #
################################################################################
#                                                                              #
#   Contributer list:                                                          #
#                                                                              #
#     Jacob Andersen, The Alexandra Institute <jacob.andersen@alexandra.dk>    #
#     Mike Kristoffersen, The Alexandra Institute <4sdc@mikek.dk>              #
#                                                                              #
################################################################################

################################################################################
# Files for FSYS library
################################################################################

#-------------------------------------------------------------------------------
# Public interface
#-------------------------------------------------------------------------------
HEADERS += \
    ../../Interface/FSYS/basemsg.h \
    ../../Interface/FSYS/handle.h \
    ../../Interface/FSYS/log.h \
    ../../Interface/FSYS/msgaddr.h \
    ../../Interface/FSYS/msgaddrgenerator.h \
    ../../Interface/FSYS/msgcallback.h \
    ../../Interface/FSYS/msgqueue.h \
    ../../Interface/FSYS/msgreceiver.h \
    ../../Interface/FSYS/msgsender.h \
    ../../Interface/FSYS/msgsenderbase.h \
    ../../Interface/FSYS/moduledeclare.h \
    ../../Interface/FSYS/modulerunner.h \
    ../../Interface/FSYS/modulethreader.h

#-------------------------------------------------------------------------------
# Private implementation
#-------------------------------------------------------------------------------
HEADERS += \
    ../../Implementation/FSYS/Common/msgqueuebase.h

SOURCES += \
    ../../Implementation/FSYS/Common/basemsg.cpp \
    ../../Implementation/FSYS/Common/handle.cpp \
    ../../Implementation/FSYS/Common/msgaddr.cpp \
    ../../Implementation/FSYS/Common/msgqueue.cpp \
    ../../Implementation/FSYS/Common/msgqueuebase.cpp \
    ../../Implementation/FSYS/Common/msgsenderbase.cpp \
    ../../Implementation/FSYS/Common/moduledeclare.cpp \
    ../../Implementation/FSYS/Common/modulerunner.cpp \
    ../../Implementation/FSYS/Common/modulethreader.cpp \
    ../../Implementation/FSYS/Qt/log.cpp


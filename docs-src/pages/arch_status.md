
Architecture Status and Road-maps     {#page_arch_status}
=================================

\if 0
******************************************************************************
***                                                                        ***
***     This file is part of the 4SDC documentation, and contains the      ***
***     page/sections with the 4SDC project status and road-map.           ***
***                                                                        ***
***        AUTHOR:   Jacob Andersen (C) The Alexandra Institute 2015       ***
***        LICENSE:  Apache 2.0                                            ***
***                                                                        ***
******************************************************************************
\endif


\if 0
[TOC]



====== Folder structure ======

If you clone the 4SDC bitbucket git project with: ''git clone https://bitbucket.org/4s/4sdcdemo.git'' then you will get a folder structure like the below:

\if 0
<code>
4SDC
├── Implementation
│   ├── 4SDC
│   ├── FSYS
│   │   ├── Common
│   │   └── Qt
│   └── PAL
│       ├── Android
│       ├── Common
│       └── Linux
├── Interface
│   ├── 4SDC
│   ├── FSYS
│   └── PAL
└── QtProjects
    ├── DemoHtmlIntegration
    ├── Shared.pro
    └── Test
</code>
\endif

As you can see there are three main folders, ''Implementation'', ''Interface'' and ''QtProjects''.  The folders under the ''QtProjects'' are Qt specific (demo's and test's), and the other two ''Implementation'' and ''Interface'' contains the actual implementation of the code and the interface to the code respectively.

===== Implementation & Interface folders =====

The ''Implementation'' and ''Interface'' folders are divided into a number of subfolders, each of these corresponds to a layer in the project.  There is currently three layers ''4SDC'', ''FSYS'' and ''PAL'', some of these layers might contain sub-modules or platform specific modules - this is especially true for the ''PAL'' layer //(You can see a description of the individual layers further down on this page)//.

If you want to integrate the project, you need to add the ''Interface'' folder to the include path in your buildsystem and the relevant files from the Implementation folder should be compiled with a C++11 conforming compiler and C++11 libraries.

You will probably only find the folders/files in the ''QtProjects'' folder interesting if you have a Qt build system, but in that case you will probably find the very interesting as we have created specific build files for each layer (these can be found in the ''QtProjects/Shared.pro'' folder.

This page is not a build guide/configuration guide, but you should be aware of the ''moduleconfig.h'' header file where the different modules are defined, and that you need to instantiate an object of the ''FSYS::ModulePrivate::ModuleThreader'' class and call the ''spawnAll()'' function on this object in order to start the system (use ''terminateAll()'' to shut it down again).  Search the sample projects/test projects to see what the content of this file can be as it is not part of the library implementation.

==== 4SDC (Session layer) ====
The ''4SDC'' or ''Session'' layer is the brain of the system, it knows about the different standards and rules for communication (e.g. the Continua IEEE 11073 standard), it is this layer that you will communicate with either directly or indirectly through the presentation layer.

**Building:** When building the Session layer, you generally want to include all the source files from ''Implementation/4SDC''.

==== PAL (Platform Abstraction Layer) ====
The PAL/Platform abstraction layer is the layer that abstracts the platforms away and leaves a common interface across all platforms for the Session layer to communicate with.  The PAL layer has different modules for each of the platforms that are supported.

**Building:** When building the platform abstraction layer you generally want to include the ''Common'' module and the module named after the platform you are building for ''Android'', ''iOS'', etc. (only choose one of the platform specific layers).

==== FSYS (4S sYStem layer) ====
FSYS is the glue that binds all of the modules together, it defines a message system so different modules can send asynchronous messages to each other (point-to-point and broadcasts), it contains module definitions and threading support.

**Building:** Currently, due to logging support, the FSYS module can only be build with a QT enabled build system, this means you need to build all of the files in the ''Implementation/FSYS'' folder.

===== QtProjects (Sample application and unit test) =====

The ''QtProjects'' folder contain various Qt5 applications and Qt related build files.

==== DemoHtmlIntegration ====

The ''DemoHtmlIntegration'' folder contains a Qt project for a sample cross platform (Linux & Android) application that allows an Html page from the Web to communicate with measurement devices.  

You can read more about the project, how to build etc. [[4sdc:demoproject:buildsetup|by following this link]].

==== Test ====

The ''Test'' folder contains a Qt project with a number of unit test for the layers in the library.

==== Shared.pro ====

The ''Shared.pro'' folder contains Qt project/build files for the ''4SDC'', ''FSYS'' and ''PAL'' layers, these are used by the other Qt projects for building.


\endif

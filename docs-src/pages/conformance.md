
Conformance Information   {#page_conformance}
=======================

\if 0
******************************************************************************
***                                                                        ***
***     This file is part of the 4SDC documentation, and contains the      ***
***     page/chapter with the information about standard conformance.      ***
***                                                                        ***
***        AUTHOR:   Jacob Andersen (C) The Alexandra Institute 2015       ***
***        LICENSE:  Apache 2.0                                            ***
***                                                                        ***
******************************************************************************
\endif


[TOC]

\todo This page is placeholder. At some point in the future, general
information and status about conformance to standards should appear
here. (Details about individual modules' conformance status should be
located in the documentation of each module and summarized here).

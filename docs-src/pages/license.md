
License    {#page_license}
=======

\if 0
******************************************************************************
***                                                                        ***
***     This file is part of the 4SDC documentation, and contains the      ***
***     page/chapter with the information about the license terms.         ***
***                                                                        ***
***        AUTHOR:   Jacob Andersen (C) The Alexandra Institute 2015       ***
***        LICENSE:  Apache 2.0                                            ***
***                                                                        ***
******************************************************************************
\endif

\addindex License
\addindex Apache 2.0 License
The 4S Device Communication Module Collection is released under the
Apache 2.0 license.

A copy of the full license text can be found below and at
[www.apache.org/licenses/LICENSE-2.0]
(http://www.apache.org/licenses/LICENSE-2.0).

\addindex Qt
Notice, however, that the _demo application_ depends on Qt libraries,
**hence the demo application will inherit the Qt license terms**. Qt
has a dual license -- you may choose between the "copy-left" LGPL
license or a commercial license, which you will have to buy from The
Qt Company. More information
\htmlonly
<a href="http://www.qt.io/FAQ/">here</a>.
\endhtmlonly
\latexonly
here: \href{http://www.qt.io/FAQ/}{\nolinkurl{www.qt.io/FAQ/}}.
\endlatexonly

\verbinclude "../LICENSE"

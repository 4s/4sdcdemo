
Contributors     {#page_contributors}
============

\if 0
******************************************************************************
***                                                                        ***
***     This file is part of the 4SDC documentation, and contains the      ***
***     page/chapter with the (legal) messages from the contributors.      ***
***                                                                        ***
***        AUTHOR:   Jacob Andersen (C) The Alexandra Institute 2015       ***
***        LICENSE:  Apache 2.0                                            ***
***                                                                        ***
******************************************************************************
\endif


The following statement lists the messages from 4S Device
Communication Module Collection contributors (the `NOTICE` file
mentioned in the license).

\verbinclude "../NOTICE"

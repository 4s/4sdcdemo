/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains implementation of the FSYS::MsgQueueBase class
 *
 * /see FSYS::MsgQueueBase
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include"FSYS/msgqueue.h"
#include"msgqueuebase.h"

void FSYS::MsgQueue::addListenerToQueue(MsgAddr &receiverAddr,
                               const std::type_info &typeOfMsg,
                               MsgCallBack *msgCallBack)
{
  FSYS::MsgQueueBase::addListenerToQueue(receiverAddr.magicKey,
                                         typeOfMsg,
                                         msgCallBack);
}

void FSYS::MsgQueue::removeListenerFromQueue(MsgCallBack *msgCallBack)
{
  FSYS::MsgQueueBase::removeListenerFromQueue(msgCallBack);
}

void FSYS::MsgQueue::waitUntilQueueHasData( void )
{
  FSYS::MsgQueueBase::waitUntilQueueHasData();
}

void FSYS::MsgQueue::emptyMsgQueue( int maxTimeMs )
{
  FSYS::MsgQueueBase::emptyMsgQueue(maxTimeMs);
}

void FSYS::MsgQueue::breakEmptyMsgQueue( void )
{
  FSYS::MsgQueueBase::breakEmptyMsgQueue();
}

bool FSYS::MsgQueue::isEmpty( void )
{
  return FSYS::MsgQueueBase::isEmpty();
}

FSYS::Handle FSYS::MsgQueue::getHandle( void )
{
  return FSYS::MsgQueueBase::getHandle();
}

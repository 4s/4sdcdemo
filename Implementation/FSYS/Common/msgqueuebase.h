/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Local include file for the message system
 *
 * Contains local declaration for the message queue system
 *
 * /see FSYS::MsgQueueBase
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef MSGQUEUEBASE_H
#define MSGQUEUEBASE_H

#include "FSYS/handle.h"
#include "FSYS/msgcallback.h"
#include "FSYS/msgaddr.h"

#include <condition_variable>
#include <forward_list>
#include <memory>
#include <mutex>
#include <queue>
#include <typeinfo>

namespace FSYS
{
  class MsgSenderBase;

  class MsgQueueBase: public Handle
  {
  friend class MsgSenderBase;
  private:
    explicit MsgQueueBase();
    ~MsgQueueBase();

    class ListenerListItem
    {
    public:
      void *magicKey;
      const std::type_info &typeOfMsg;
      MsgCallBack * msgCallBack;
      ListenerListItem( void *magicKey,
                        const std::type_info &typeOfMsg,
                        MsgCallBack * msgCallBack)
        : magicKey(magicKey),
          typeOfMsg(typeOfMsg),
          msgCallBack(msgCallBack)
      {}
    };

    class MsgQueueItem
    {
    public:
      std::shared_ptr<BaseMsg> msg;  // Msg to queue
      const std::type_info &typeOfMsg;
      Handle object;                 // Msg object to target
      void *magicKey;                   // MagicKey to target
      MsgQueueItem(std::shared_ptr<BaseMsg> msg,
                   const std::type_info &typeOfMsg,
                   Handle object,
                   void *magicKey)
        : msg(msg),
          typeOfMsg(typeOfMsg),
          object(object),
          magicKey(magicKey)
      {}

      MsgQueueItem(const MsgQueueItem &msgQueueItem)
        : msg(msgQueueItem.msg),
          typeOfMsg(msgQueueItem.typeOfMsg),
          object(msgQueueItem.object),
          magicKey(msgQueueItem.magicKey)
      {}
    };

    std::condition_variable_any localQueueConditionVariable;
    std::queue<MsgQueueItem> localMsgQueue;

    inline static std::recursive_mutex &getMutex( void )
    {
      static std::recursive_mutex mutex;
      return mutex;
    }

    std::forward_list<ListenerListItem> localListenerList;

    // The list of queues in the system
    static std::forward_list<MsgQueueBase*> listOfQueues;

    /**
     * @brief Adds the message to the right queues
     *
     * @param msg The message to send
     */
    static void sendMsg(std::shared_ptr<BaseMsg> msg,  // Msg to send
                        const std::type_info &typeOfMsg,  // Type of msg to send                        
                        Handle queue,                     // Msg queue to target
                        Handle object,                    // Msg object to target
                        void *magicKey                    // MagicKey to target
                       );




    inline static MsgQueueBase &getThreadQueue( void )
    {
      static thread_local MsgQueueBase localQueue;
      return localQueue;
    }


    inline static bool &getTerminateLoopFlag( void )
    {
      // Set to true when loop should be terminated
      static thread_local bool terminateFlag;

      return terminateFlag;
    }

  public:

    inline static Handle getBroadCastHandle( void )
    {
      static Handle broadcastHandle;
      return broadcastHandle;
    }


    static void addListenerToQueue(void *magicKey,
                                   const std::type_info &typeOfMsg,
                                   MsgCallBack *msgCallBack);

    static void removeListenerFromQueue(MsgCallBack *msgCallBack);



    static void waitUntilQueueHasData( void );

    /**
     * @brief The function will process messages on the message queue
     *
     * This function is used for processing messages from the message queue
     * of the thread that this function is called from.
     *
     * The default behavior is for the function to run until there are no
     * more messages on the queue at which point the function will return.
     *
     * It is possible to give an optional argument to the function limiting
     * the time it will run.
     *
     * @param maxTimeMs  The maximum time in MS that will pass before the
     *                   function breaks the loop that empties the queue.
     *
     *                   maxTimeMs <  0 => Run until the queue is empty or
     *                                     breakEmptyMsgQueue() is called
     *
     *                   maxTimeMS == 0 => Run until all messages on the queue
     *                                     at the time of the call are processed
     *
     *                   maxTimeMS >  0 => Run approximately until maxTimeMS
     *                                     have passed
     */
    static void emptyMsgQueue( int maxTimeMs=-1 );



    /**
     * @brief Breaks out of emptyMsgQueue
     *
     * Function must be called from a message handler (directly or indirectly)
     * and will cause the message loop internal to the emptyMsgQueue function to
     * break.
     *
     * This function is used in the case where you have a component that as part
     * of its operation sends messages to it self.
     *
     * Usually the emptyMsgQueue function will only return when the message is
     * queue is empty, but in the case where components on the queue keeps
     * sending to other components in the same thread, the queue will never be
     * empty and hence will never terminate.
     *
     * Calling this function will break the msg loop.
     */
    static void breakEmptyMsgQueue( void );

    /**
     * @brief Function to determine if there is data on the msg queue
     *
     * Call this function to determine if there are messages waiting to be
     * processed on the queue.
     *
     * @return true if there is messages waiting to be processd, false if not
     */
    static bool isEmpty( void );

    /**
     * @brief Function to get the handle for this threads message queue
     *
     * @return The handle for this threads message queue.
     */
    inline static Handle getHandle( void )
    {
      return getThreadQueue();
    }

  };

}

#endif // MSGQUEUEBASE_H

/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains implementation of the FSYS::MsgSenderBase class
 *
 * /see FSYS::MsgSenderBase
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "FSYS/msgsenderbase.h"

#include "FSYS/basemsg.h"
#include "FSYS/msgqueue.h"
#include "msgqueuebase.h"

#include <assert.h>


std::shared_ptr<FSYS::BaseMsg> &FSYS::MsgSenderBase::fillOutSenderInfo(std::shared_ptr<BaseMsg> &msg,
                                                                       MsgAddr sender)
{
  // Make sure this message hasn't been sent before
  assert(!msg->getOriginAddr().isValid());

  // Fill out the sender info
  msg->setOriginAddr(sender);

  return msg;
}

void FSYS::MsgSenderBase::broadcast(std::shared_ptr<BaseMsg> &msg,
                                    const std::type_info &typeOfMsg,
                                    const MsgAddr &sender)
{
  // Broadcast the message
  send(msg, typeOfMsg, sender, FSYS::MsgAddr::broadcastAddr());
}

void FSYS::MsgSenderBase::send(std::shared_ptr<BaseMsg> &msg,
                               const std::type_info &typeOfMsg,
                               const MsgAddr &sender,
                               const MsgAddr &destination)
{
  // Send the message to specific
  MsgQueueBase::sendMsg(fillOutSenderInfo(msg, sender),// Msg to send
                        typeOfMsg,                     // Type of msg being sent                        
                        destination.queue,
                        destination.object,         // Who the message should be sent to
                        destination.magicKey
                        );
}




/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains implementation of the class(es) related to the module runner
 *
 * The Runner class is a class that contains the modules, and makes sure they
 * are started in the right order and a controlled way.
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "FSYS/log.h"
#include "FSYS/modulerunner.h"
#include "FSYS/moduledeclare.h"


DECLARE_LOG_MODULE("ModuleRunner");



FSYS::ModulePrivate::Runner::Runner(std::string groupName,
             std::initializer_list<LauncherBase*> moduleList)
{
  // Copy the group name
  this->groupName = groupName;

  // Copy the entries in the module list to the groupModuleList  
  for(auto element: moduleList)
  {
    groupModuleList.push_back(element);
  }
}

void FSYS::ModulePrivate::Runner::startStaticModules( void )
{
  // Go through each element in the list
  for(auto element: groupModuleList)
  {
    if(LauncherBase::ModuleType::STATIC == element->getType())
    {
      element->startup();
    }
  }

  // After all static modules are started, we can tell them the group is ready
  for(auto element: groupModuleList)
  {
    element->sendMsgGroupReady();
  }

  // Broadcast that we are ready to enter our message loop
  MsgGroupReady groupReady;
  broadcast(groupReady);
}

void FSYS::ModulePrivate::Runner::runMsgLoop( void )
{
  // Run the message loop until MsgDestroy is received
  while(!terminateMsgLoop)
  {
    MsgQueue::waitUntilQueueHasData();
    MsgQueue::emptyMsgQueue(0);
  }
}

void FSYS::ModulePrivate::Runner::shutdown( void )
{
  // Loop through the list
  for(auto element: groupModuleList)
  {
    // Shutdown each element
    element->shutdown();
    delete element;
  }

  // Delete the list
  groupModuleList.clear();
}


void FSYS::ModulePrivate::Runner::receive(MsgSystemReady)
{
  for(auto element: groupModuleList)
  {
    element->sendMsgSystemReady();
  }
}

void FSYS::ModulePrivate::Runner::receive(MsgGoingDown)
{
  for(auto element: groupModuleList)
  {
    element->sendMsgGoingDown();
  }
}

void FSYS::ModulePrivate::Runner::receive(MsgDestroy)
{
  // Terminate the modules
  shutdown();

  // Remeber the message loop should be terminated
  terminateMsgLoop = true;
}

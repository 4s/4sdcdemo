/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains implementation of the classes related to module definitions
 *
 * /see FSYS::Handle
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "FSYS/moduledeclare.h"

#include "assert.h"

void FSYS::ModulePrivate::LauncherBase::sendMsgReady( void )
{
  if(isRunning()) userObject->msgReady();
}

void FSYS::ModulePrivate::LauncherBase::sendMsgGroupReady( void )
{
  if(isRunning()) userObject->msgGroupReady();
}

void FSYS::ModulePrivate::LauncherBase::sendMsgSystemReady( void )
{
  if(isRunning()) userObject->msgSystemReady();
}

void FSYS::ModulePrivate::LauncherBase::sendMsgGoingDown( void )
{
  if(isRunning()) userObject->msgGoingDown();
}

void FSYS::ModulePrivate::LauncherBase::sendMsgDestroy( void )
{
  if(isRunning()) userObject->msgDestroy();
}

bool FSYS::ModulePrivate::LauncherBase::isRunning( void )
{
  return isCreated;
}


void FSYS::ModulePrivate::LauncherBase::startup( void )
{
  // Only start if the module hasn't been started allready
  if(!isRunning())
  {
    // First create the object
    create();

    // Remember the module has been created
    isCreated = true;

    // Tell the object it has been created
    sendMsgReady();
  }
}

void FSYS::ModulePrivate::LauncherBase::shutdown( void )
{
  // Only shut down if the module is running
  if(isRunning())
  {
    // Give the object a last chance to say goodbye
    sendMsgDestroy();

    // Destroy the object
    destroy();

    // Remember the module has been destroyed
    isCreated = false;
  }
}

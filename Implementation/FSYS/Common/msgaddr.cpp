/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains implementation of the FSYS::MsgAddr class
 *
 * /see FSYS::MsgAddr
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "FSYS/msgaddr.h"
#include "msgqueuebase.h"

void FSYS::MsgAddr::clear( void )
{
  object = Handle::null();
  queue = Handle::null();
  magicKey = nullptr;
}

FSYS::MsgAddr::MsgAddr( void )
  : object(Handle::null()),
    queue(Handle::null()),
    magicKey(nullptr)
{
}

FSYS::MsgAddr::MsgAddr(Handle object, Handle queue, void * magicKey)
  : object(object),
    queue(queue),
    magicKey(magicKey)
{
}

bool FSYS::MsgAddr::isValid( void )
{
  return Handle::null() != object
      && Handle::null() != queue
      && nullptr != magicKey;
}

FSYS::MsgAddr &FSYS::MsgAddr::broadcastAddr( void )
{
  static MsgAddr broadcast(
        MsgQueueBase::getBroadCastHandle(), // All queues
        MsgQueueBase::getBroadCastHandle(), // All objects
        nullptr                       // Don't care about magic
        );

  return broadcast;
}

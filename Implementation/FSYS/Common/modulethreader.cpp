/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains implementation of the class(es) related to the module threader
 *
 * The Threader class is the class that controls the threads in the system
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "FSYS/log.h"
#include "FSYS/modulerunner.h"
#include "FSYS/moduledeclare.h"
#include "FSYS/modulethreader.h"
#include "FSYS/msgreceiver.h"
#include "FSYS/msgsender.h"

#include <vector>
#include <thread>
#include <algorithm>

DECLARE_LOG_MODULE("ModuleThreader");

#define MODULE_GROUP(gROUPnAME)
#define MODULE_STATIC(mODULEcLASSnAME)
#define MODULE_GROUP_END

#include "moduleconfig.h"

#undef MODULE_GROUP
#undef MODULE_STATIC
#undef MODULE_GROUP_END

#define MODULE_GROUP(gROUPnAME) threadKeeper.threads.push_back(std::thread([]() \
                                { \
                                  FSYS::ModulePrivate::Runner runner(gROUPnAME,{

#define MODULE_STATIC(mODULEcLASSnAME) new FSYS::ModulePrivate::Launcher<mODULEcLASSnAME>,

#define MODULE_GROUP_END               new FSYS::ModulePrivate::LauncherEnd} \
                                  ); \
                                  runner.startStaticModules(); \
                                  runner.runMsgLoop(); \
                                }));

namespace
{
  class ModuleThreadKeeper : public FSYS::MsgSender<ModuleThreadKeeper>,
                             public FSYS::MsgReceiver<ModuleThreadKeeper,
                                                      FSYS::MsgGroupReady>,
                             public FSYS::MsgReceiver<ModuleThreadKeeper,
                                                      FSYS::MsgDestroy>
  {
  private:
    unsigned int noOfMsgGroupReadyReceived = {0};

  public:
    std::vector<std::thread> threads;

    void receive(FSYS::MsgGroupReady)
    {
      noOfMsgGroupReadyReceived++;

      // Check if we have gotten a ready from all groups
      if(threads.size() == noOfMsgGroupReadyReceived)
      {
        // If we have then broadcast the system ready
        FSYS::MsgSystemReady ready;
        broadcast(ready);
      }
    }


    void receive(FSYS::MsgDestroy)
    {
      // Wait for all threads to terminate
      std::for_each(threads.begin(), threads.end(), [](std::thread &thread)
      {
        thread.join();
      });

      // Clear the threads list
      threads.clear();

      // Tell the world that all threads have been destroyed
      FSYS::MsgAllDestroyed allDestroyed;
      broadcast(allDestroyed);
    }

    void littleSleep(std::chrono::microseconds us)
    {
        auto start = std::chrono::high_resolution_clock::now();
        auto end = start + us;
        do {
            std::this_thread::yield();
        } while (std::chrono::high_resolution_clock::now() < end);
    }

    void terminate( void )
    {
      // Give all threads a warning they are being terminated
      FSYS::MsgGoingDown goingDown;
      broadcast(goingDown);

      // Wait a little to give the other threads a chance to clean-up
      littleSleep(std::chrono::microseconds(500));

      // Tell all threads to terminate
      FSYS::MsgDestroy destroy;
      broadcast(destroy);
      receive(destroy);

      noOfMsgGroupReadyReceived = 0;
    }

  } threadKeeper;
}

void FSYS::ModulePrivate::ModuleThreader::spawnAll( void )
{
/*
  threads.push_back(std::thread([](){
      FSYS::ModulePrivate::Runner runner("Test checkRunnerInit",
                                         {new FSYS::ModulePrivate::Launcher<ModuleA>,
                                          new FSYS::ModulePrivate::Launcher<ModuleA>,
                                          new FSYS::ModulePrivate::Launcher<ModuleB>,
                                          new FSYS::ModulePrivate::Launcher<ModuleA>});
      runner.startStaticModules();
      runner.runMsgLoop();
    }));
  threads.push_back(std::thread([](){
      FSYS::ModulePrivate::Runner runner("Test checkRunnerInit",
                                         {new FSYS::ModulePrivate::Launcher<ModuleA>,
                                          new FSYS::ModulePrivate::Launcher<ModuleA>,
                                          new FSYS::ModulePrivate::Launcher<ModuleB>,
                                          new FSYS::ModulePrivate::Launcher<ModuleA>});
      runner.startStaticModules();
      runner.runMsgLoop();
    }));
*/
  #include "moduleconfig.h"
}

void FSYS::ModulePrivate::ModuleThreader::terminateAll( void )
{
  threadKeeper.terminate();
}


/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Tiny helper class used to find the Java ClassLoader
 *
 * This tiny class is used by android.cpp to acquire a reference to the Java
 * ClassLoader which must be used to load Java classes from native code.
 *
 * @see android.cpp
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob Andersen</a>,
 *         The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

package dk._4s_online.device_communication.PAL.Android;

/**
 * @brief Tiny helper class used to find the Java ClassLoader
 *
 * This tiny class is used by android.cpp to acquire a reference to the Java
 * ClassLoader which must be used to load Java classes from native code.
 *
 * @see android.cpp
 */
class GetClassLoader {

    /**
     * @brief Get the ClassLoader
     *
     * Acquire the java.lang.ClassLoader object which will be able to load
     * the project classes.
     *
     * @return The java.lang.ClassLoader object
     */
    static ClassLoader get() {
        return GetClassLoader.class.getClassLoader();
    }

}

/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Handles to the Java VM running our Android service.
 *
 * This component bridges the C++ and Java worlds of the Android service by
 * providing the handles into the Android/Java world needed by the C++
 * components of the PAL layer.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute, &copy; 2015
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "PAL/android.h"
#include "FSYS/log.h"

#include <cassert>

DECLARE_LOG_MODULE("Android")

using PAL::Android;

static JavaVM *theVM = nullptr;
static jobject classLoader = nullptr;
static jobject androidContext = nullptr;

/**
 * @brief JNI initialization function
 *
 * This function is called by the JNI (or the Qt library when used in a
 * QtAndroid context) when the library has been loaded and should initialize
 * itself.
 *
 * We use it to save references to the JavaVM and the Java ClassLoader to
 * use for loading 4S Java classes.
 *
 * @param vm The JavaVM pointer.
 */
extern "C" jint JNI_OnLoad(JavaVM *vm, void*) {

    // Test that JNIVER is supported by the Java VM.
    // (if it does not, bail out).

    JNIEnv *env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNIVER) != JNI_OK) {
        return -1;
    }

    // Acquire a global reference to a classloader that will be capable of
    // loading PAL-layer classes.

    jclass cls = env->FindClass(
               "dk/_4s_online/device_communication/PAL/Android/GetClassLoader");

    // Fail fast if we are debugging
    assert(!env->ExceptionCheck() && cls);
    // Otherwise: log the event and bail out
    if (env->ExceptionCheck()) {
        env->ExceptionDescribe();
        env->ExceptionClear();
        return -1;
    } else if (!cls) {
        ERR("Unexpected error fetching ClassLoader reference.");
        return -1;
    }

    jmethodID methodId = env->GetStaticMethodID(cls, "get",
                                                "()Ljava/lang/ClassLoader;");

    assert(!env->ExceptionCheck() && methodId);
    if (env->ExceptionCheck()) {
        env->ExceptionDescribe();
        env->ExceptionClear();
        env->DeleteLocalRef(cls);
        return -1;
    } else if (!methodId) {
        ERR("Unexpected error fetching ClassLoader reference.");
        env->DeleteLocalRef(cls);
        return -1;
    }

    jobject localClassLoaderRef = env->CallStaticObjectMethod(cls, methodId);

    assert(!env->ExceptionCheck() && localClassLoaderRef);
    if (env->ExceptionCheck()) {
        env->ExceptionDescribe();
        env->ExceptionClear();
        env->DeleteLocalRef(cls);
        return -1;
    } else if (!localClassLoaderRef) {
        ERR("Unexpected error fetching ClassLoader reference.");
        env->DeleteLocalRef(cls);
        return -1;
    }

    // Save a global reference to the ClassLoader object for later
    classLoader = env->NewGlobalRef(localClassLoaderRef);
    assert(classLoader); // Fail fast if we are debugging
    if (!classLoader) {
        ERR("Out-of-memory error fetching ClassLoader reference.");
        env->DeleteLocalRef(localClassLoaderRef);
        env->DeleteLocalRef(cls);
        return -1;
    }

    // Everything went well. Return succesfully.
    env->DeleteLocalRef(localClassLoaderRef);
    env->DeleteLocalRef(cls);
    theVM = vm;
    return JNIVER;
}

/**
 * @brief JNI deinitialization function
 *
 * This function is called by the JNI (or the Qt library when used in a
 * QtAndroid context) when the library is being unloaded.
 *
 * We use it to free the references saved in the JNI_OnLoad() function.
 *
 * @param vm The JavaVM pointer.
 */
extern "C" void JNI_OnUnload(JavaVM *vm, void*) {
    theVM = nullptr;
    if (!classLoader && !androidContext) return;
    JNIEnv *env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNIVER) != JNI_OK) return;

    if (classLoader) {
        env->DeleteGlobalRef(classLoader);
        classLoader = nullptr;
    }
    if (androidContext) {
        env->DeleteGlobalRef(androidContext);
        androidContext = nullptr;
    }
}

JavaVM *Android::getJavaVM() {
    assert(theVM);
    return theVM;
}

jobject Android::getClassLoader() {
    assert(classLoader);
    return classLoader;
}

void Android::setContext(jobject contextRef) {
    // This function must be called exactly once during initialization
    assert(!androidContext);
    JNIEnv *env = nullptr;
    // Test if this thread is attached to the JavaVM
    switch (theVM->GetEnv(reinterpret_cast<void**>(&env), JNIVER)) {
    case JNI_OK:
        // The thread was already attached to the JavaVM.
        assert(env);
        // Save a global reference to the ClassLoader object for later
        androidContext = env->NewGlobalRef(contextRef);
        break;
    case JNI_EDETACHED:
        // The thread was not attached to the JavaVM: Attach and then detach
        {
            class Attacher {
                JavaVM *vm;
            public:
                Attacher(JavaVM *vm, JNIEnv **envptr) : vm(vm)
                            { vm->AttachCurrentThread(envptr,NULL); }
                ~Attacher() { vm->DetachCurrentThread();            }
            } attacher(theVM,&env);
            assert(env);
            // Save a global reference to the ClassLoader object for later
            androidContext = env->NewGlobalRef(contextRef);
        }
        break;
    default:
        // Unexpected error
        assert(0); // Should never happen
        return;
    }

    assert(androidContext); // Fail fast if we are debugging
    if (androidContext == nullptr) {
        ERR("Out-of-memory error creating Context reference.");
    }
}

jobject Android::getContext() {
    assert(androidContext);
    return androidContext;
}



// Qt alternative version below. Can hopefully be avoided (and then deleted)
#if 0
#include "FSYS/log.h"

#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>

#include <cassert>

DECLARE_LOG_MODULE("Android")


using PAL::Android;

static class ClassLoader {
    // Global Java object reference to the ClassLoader
    jobject cl = nullptr;
public:
    ClassLoader() {
        QAndroidJniEnvironment env;

        QAndroidJniObject localobj = QAndroidJniObject::callStaticObjectMethod(
                "dk/_4s_online/device_communication/PAL/Android/GetClassLoader",
                "get","()Ljava/lang/ClassLoader;");
        // Fail fast if we are debugging
        assert(!env->ExceptionCheck() && localobj != nullptr);
        // Otherwise: log the event and bail out - hoping that the error can be
        // properly detected and reported later
        if (env->ExceptionCheck()) {
            env->ExceptionDescribe();
            env->ExceptionClear();
            return;
        } else if (localobj == nullptr) {
            ERR("Unexpected error fetching ClassLoader reference.");
            return;
        }

        // Save a global reference to the ClassLoader object for later
        cl = env->NewGlobalRef(localobj.object());
        assert(cl); // Fail fast if we are debugging
        if (cl == nullptr) {
            ERR("Out-of-memory error fetching ClassLoader reference.");
            return;
        }
    }
    ~ClassLoader() {
        assert(cl);
        QAndroidJniEnvironment env;
        env->DeleteGlobalRef(cl); // Free the global reference
    }
    jobject get() {
        assert(cl);
        return cl;
    }
} classLoader;

static jobject context = nullptr;

JavaVM *Android::getJavaVM() {
    return QAndroidJniEnvironment::javaVM();
}

jobject Android::getClassLoader() {
    return classLoader.get();
}

void Android::setContext(jobject contextRef) {
    // This function must be called exactly once during initialization
    assert(!context);
    QAndroidJniEnvironment env;
    // Save a global reference to the ClassLoader object for later
    context = env->NewGlobalRef(contextRef);
    assert(context); // Fail fast if we are debugging
    if (context == nullptr) {
        ERR("Out-of-memory error creating Context reference.");
    }
}

jobject Android::getContext() {
    assert(context);
    return context;
}

#endif


/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Android PAL bluetooth implementation using the Bluedroid stack.
 *
 * This file implements the bluetooth PAL module specified in bluetooth.h
 * for the Android platform using the Bluedroid bluetooth stack. Communication
 * with the Java-based stack goes through the Bluedroid.java class.
 * 
 * Currently, only Bluetooth classic is supported, but in the future,
 * also Bluetooth LE (a.k.a. SMART) support should be covered by this
 * implementation.
 *
 * @note This module will spawn threads to handle Java/JNI tasks and
 *       communication. One of these threads will be an
 *       FSYS::MsgSender, but none will be receivers; hence, none will
 *       be running a message dispatcher. This module will therefore
 *       be able to work in a single-dispatcher environment, where no
 *       \c thread_local storage is needed.
 *
 * @see Bluedroid.java
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob Andersen</a>,
 *         The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "PAL/bluetooth.h"
#include "PAL/android.h"
#include "FSYS/log.h"
#include "FSYS/basemsg.h"
#include "FSYS/msgaddr.h"
#include "FSYS/msgreceiver.h"
#include "FSYS/msgsender.h"

#include <cassert>
#include <condition_variable>
#include <jni.h>
#include <map>
#include <mutex>
#include <queue>
#include <set>
#include <string>
#include <thread>
#include <typeinfo>
#include <memory>
#include <utility>
#include <vector>

DECLARE_LOG_MODULE("Bluedroid")


using std::condition_variable;
using std::lock_guard;
using std::map;
using std::mutex;
using std::pair;
using std::queue;
using std::set;
using std::string;
using std::thread;
using std::shared_ptr;
using std::unique_lock;
using std::vector;

using PAL::BluetoothModule;
using PAL::PHDBluetoothDevice;
using PAL::PhysicalPHD;
using PAL::VirtualPHD;
using PAL::Android;
using FSYS::BaseMsg;
using FSYS::Handle;
using FSYS::MsgAddr;
using FSYS::MsgReceiver;
using FSYS::MsgSender;



/* ************************************************************ */
/*              Private classes used by the handler             */
/* ************************************************************ */

// We keep them private to this file...
namespace {

//
//  Messages from Java -> C++ sent from the javaThread to the module thread
//

class ConnectIndicationMsg : public BaseMsg {
public:
    string address;
    uint16_t datatype;
    string displayName; // May equal serviceName
    string serviceName;
    string mdepDescription; // May be set to ""
};

class DisconnectIndicationMsg : public BaseMsg {
public:
    string address;
    uint16_t datatype;
    errortype error;
};

class ReceiveApduMsg : public BaseMsg {
public:
    string address;
    uint16_t datatype;
    vector<uint8_t> apdu;
    bool reliable;
};


//
//  Messages from C++ -> Java sent from the module thread to the cppThread
//

struct DownstreamRequest {
    virtual ~DownstreamRequest() {}
};

struct InterruptRequest : public DownstreamRequest {};

struct DatatypeRequest : public DownstreamRequest {
    uint16_t datatype;
    bool added;
    DatatypeRequest(uint16_t datatype, bool added) : datatype(datatype),
                                                                 added(added) {}
};

struct DisconnectRequest : public DownstreamRequest {
    string address;
    uint16_t datatype;
    DisconnectRequest(string address, uint16_t datatype) : address(address),
                                                           datatype(datatype) {}
};

struct SendApduRequest : public DownstreamRequest {
    string address;
    uint16_t datatype;
    vector<uint8_t> apdu;
    bool primary;
    SendApduRequest(string address, uint16_t datatype, vector<uint8_t> apdu,
                    bool primary) : address(address), datatype(datatype),
                                    apdu(apdu), primary(primary) {}
};


// Forward declaration of function to register native JNI methods
static void jniRegisterNatives(JNIEnv *env, jclass cls);


//
//  The JNI manager handling all Java-world communication back and forth.
//

class JniManager : MsgSender<JniManager> {

    JNIEnv *javaEnv = nullptr;
    JNIEnv *cppEnv = nullptr;

    jobject subsystem = nullptr;
    MsgAddr &sink;

    jmethodID run, interruptRequest, datatypeRequest,
              disconnectRequest, sendApduRequest;

public:

    JniManager(MsgAddr &sink) : sink(sink) {

        JavaVM *vm = Android::getJavaVM();

        // Attach this thread to the JVM (creating a Java Thread object)

        // Give the Java Thread instance a name to ease debugging
        JavaVMAttachArgs args = {JNIVER,
                                 "4S.PAL.Bluedroid: Java -> C++ Handler",
                                 NULL};

        if (vm->AttachCurrentThread(&javaEnv, &args) != JNI_OK) {
            FATAL("Could not attach thread");
            assert(false);
        }
        assert(javaEnv);
        DEBUG("Attached javaThread");

        // Create the Java Bluedroid object which manages the Java part of the
        // subsystem

        // ClassLoader.getSystemClassLoader()
        jclass cls = javaEnv->FindClass("java/lang/ClassLoader");
        assert(cls && !javaEnv->ExceptionCheck());
        jmethodID method = javaEnv->GetMethodID(cls, "loadClass",
                                       "(Ljava/lang/String;)Ljava/lang/Class;");
        javaEnv->DeleteLocalRef(cls);
        assert(method && !javaEnv->ExceptionCheck());

        cls = (jclass)(javaEnv->CallObjectMethod(Android::getClassLoader(),
                  method,javaEnv->NewStringUTF(
                  "dk._4s_online.device_communication.PAL.Android.Bluedroid")));
        if (cls == NULL || javaEnv->ExceptionCheck()) {
            FATAL("Could not link to the Bluedroid class");
            assert(false);
        }

        // Register native methods (Java -> C++)
        jniRegisterNatives(javaEnv, cls);

        // Register Java methods (C++ -> Java)
        run = javaEnv->GetMethodID(cls, "run", "()V");
        if (run == NULL || javaEnv->ExceptionCheck()) {
            FATAL("Could not link to the Bluedroid class");
            assert(false);
        }
        interruptRequest = javaEnv->GetMethodID(cls, "interruptRequest", "()V");
        if (interruptRequest == NULL || javaEnv->ExceptionCheck()) {
            FATAL("Could not link to the Bluedroid class");
            assert(false);
        }
        datatypeRequest = javaEnv->GetMethodID(cls, "datatypeRequest",
                                               "(CZ)V");
        if (datatypeRequest == NULL || javaEnv->ExceptionCheck()) {
            FATAL("Could not link to the Bluedroid class");
            assert(false);
        }
        disconnectRequest = javaEnv->GetMethodID(cls, "disconnectRequest",
                                                 "(Ljava/lang/String;C)V");
        if (disconnectRequest == NULL || javaEnv->ExceptionCheck()) {
            FATAL("Could not link to the Bluedroid class");
            assert(false);
        }
        sendApduRequest = javaEnv->GetMethodID(cls, "sendApduRequest",
                                               "(Ljava/lang/String;C[BZ)V");
        if (sendApduRequest == NULL || javaEnv->ExceptionCheck()) {
            FATAL("Could not link to the Bluedroid class");
            assert(false);
        }

        // Invoke the constructor and create the Bluedroid Java object
        //assert(Android::getContext()); // FIXME uncomment assert
        method = javaEnv->GetMethodID(cls, "<init>",
                                      "(Landroid/content/Context;)V");
        if (method == NULL || javaEnv->ExceptionCheck()) {
            FATAL("Could not link to the Bluedroid class");
            assert(false);
        }
        jobject obj = javaEnv->NewObject(cls, method, Android::getContext());
        javaEnv->DeleteLocalRef(cls);
        if (obj == NULL || javaEnv->ExceptionCheck()) {
            FATAL("Could not link to the Bluedroid class");
            assert(false);
        }
        subsystem = javaEnv->NewGlobalRef(obj);
        javaEnv->DeleteLocalRef(obj);
        if (subsystem == NULL) {
            FATAL("Could not link to the Bluedroid class");
            assert(false);
        }
        DEBUG("Bluedroid Java subsystem created");
    }

    ~JniManager() {
        JavaVM *vm = Android::getJavaVM();
        assert(javaEnv);
        assert(!cppEnv);
        assert(subsystem);

        // Release the Java Bluedroid object, so it can be garbage-collected
        javaEnv->DeleteGlobalRef(subsystem);
        subsystem = nullptr;
        DEBUG("Bluedroid Java subsystem released");

        // Detach this thread from the JVM
        if (vm->DetachCurrentThread() != JNI_OK) {
            FATAL("Could not attach thread");
            assert(false);
        }
        javaEnv = nullptr;
        DEBUG("Detached javaThread");
    }


    // This function will execute the Java dispatcher. This method will be
    // executed on the javaThread and will not return until interrupted.
    void runJavaDispatcher() {
        assert(javaEnv);
        DEBUG("Entering javaThread dispatcher");
        javaEnv->CallVoidMethod(subsystem,run);
        if (javaEnv->ExceptionCheck()) {
            ERR("Unhandled exception in javaThread dispatcher");
            assert(false);
        }
        DEBUG("Exiting javaThread dispatcher");
    }

    //
    // The following methods are all called on the cppThread
    //

    void attachThread() {
        JavaVM *vm = Android::getJavaVM();
        assert(!cppEnv);

        // Attach this thread to the JVM (creating a Java Thread object)

        // Give the Java Thread instance a name to ease debugging
        JavaVMAttachArgs args = {JNIVER,
                                 "4S.PAL.Bluedroid: C++ -> Java Handler",
                                 NULL};

        if (vm->AttachCurrentThread(&cppEnv, &args) != JNI_OK) {
            FATAL("Could not attach thread");
            assert(false);
        }
        assert(cppEnv);
        DEBUG("Attached cppThread");
    }

    void detachThread() {
        JavaVM *vm = Android::getJavaVM();
        assert(cppEnv);

        // Detach this thread from the JVM
        if (vm->DetachCurrentThread() != JNI_OK) {
            FATAL("Could not attach thread");
            assert(false);
        }
        cppEnv = nullptr;
        DEBUG("Detached cppThread");
    }

    void interruptReq() {
        assert(cppEnv);
        cppEnv->CallVoidMethod(subsystem, interruptRequest);
        if (cppEnv->ExceptionCheck()) {
            ERR("Unhandled exception in interruptRequest");
            assert(false);
        }
    }

    void datatypeReq(uint16_t datatype, bool added) {
        assert(cppEnv);
        cppEnv->CallVoidMethod(subsystem, datatypeRequest, datatype, added);
        if (cppEnv->ExceptionCheck()) {
            ERR("Unhandled exception in datatypeRequest");
            assert(false);
        }
    }

    void disconnectReq(string address, uint16_t datatype) {
        assert(cppEnv);
        jstring addr = cppEnv->NewStringUTF(address.c_str());
        if (addr == NULL || cppEnv->ExceptionCheck()) {
            ERR("Unhandled exception in disconnectRequest");
            assert(false);
        }
        cppEnv->CallVoidMethod(subsystem, disconnectRequest, addr, datatype);
        if (cppEnv->ExceptionCheck()) {
            ERR("Unhandled exception in disconnectRequest");
            assert(false);
        }
        cppEnv->DeleteLocalRef(addr);
    }

    void sendApduReq(string address, uint16_t datatype,
                     vector<uint8_t> &&apdu, bool primary) {
        assert(cppEnv);
        jstring addr = cppEnv->NewStringUTF(address.c_str());
        if (addr == NULL || cppEnv->ExceptionCheck()) {
            ERR("Unhandled exception in sendApduRequest");
            assert(false);
        }
        jbyteArray japdu = cppEnv->NewByteArray(apdu.size());
        if (japdu == NULL || cppEnv->ExceptionCheck()) {
            ERR("Unhandled exception in sendApduRequest");
            assert(false);
        }
        cppEnv->SetByteArrayRegion(japdu,0,apdu.size(),(jbyte*)apdu.data());
        if (cppEnv->ExceptionCheck()) {
            ERR("Unhandled exception in sendApduRequest");
            assert(false);
        }
        cppEnv->CallVoidMethod(subsystem, sendApduRequest, addr, datatype,
                               japdu, primary);
        if (cppEnv->ExceptionCheck()) {
            ERR("Unhandled exception in sendApduRequest");
            assert(false);
        }
        cppEnv->DeleteLocalRef(addr);
        cppEnv->DeleteLocalRef(japdu);
    }

    void connectIndication(JNIEnv *env, jobject thiz, jstring address,
                           jchar datatype, jstring displayName,
                           jstring serviceName, jstring mdepDescription) {
        // Assert a sane environment
        assert(env == javaEnv && subsystem &&
               env->IsSameObject(thiz,subsystem));

        ConnectIndicationMsg msg;

        const char *s = env->GetStringUTFChars(address, NULL);
        assert(s);
        msg.address = s;
        env->ReleaseStringUTFChars(address,s);

        msg.datatype = datatype;

        s = env->GetStringUTFChars(displayName, NULL);
        assert(s);
        msg.displayName = s;
        env->ReleaseStringUTFChars(displayName,s);

        s = env->GetStringUTFChars(serviceName, NULL);
        assert(s);
        msg.serviceName = s;
        env->ReleaseStringUTFChars(serviceName,s);

        s = env->GetStringUTFChars(mdepDescription, NULL);
        assert(s);
        msg.mdepDescription = s;
        env->ReleaseStringUTFChars(mdepDescription,s);

        send(msg, sink);
    }

    void disconnectIndication(JNIEnv *env, jobject thiz, jstring address,
                              jchar datatype, jint error) {
        // Assert a sane environment
        assert(env == javaEnv && subsystem &&
               env->IsSameObject(thiz,subsystem));

        DisconnectIndicationMsg msg;

        const char *s = env->GetStringUTFChars(address, NULL);
        assert(s);
        msg.address = s;
        env->ReleaseStringUTFChars(address,s);

        msg.datatype = datatype;

        msg.error = error;

        send(msg, sink);
    }

    void messageReceived(JNIEnv *env, jobject thiz, jstring address,
                         jchar datatype, jbyteArray apdu,
                         jboolean reliable) {
        // Assert a sane environment
        assert(env == javaEnv && subsystem &&
               env->IsSameObject(thiz,subsystem));

        ReceiveApduMsg msg;

        const char *s = env->GetStringUTFChars(address, NULL);
        assert(s);
        msg.address = s;
        env->ReleaseStringUTFChars(address,s);

        msg.datatype = datatype;

        int8_t *bytes = env->GetByteArrayElements(apdu, NULL);
        assert(bytes);
        msg.apdu.assign(reinterpret_cast<uint8_t*>(bytes),
                 reinterpret_cast<uint8_t*>(bytes) + env->GetArrayLength(apdu));
        env->ReleaseByteArrayElements(apdu, bytes, JNI_ABORT);

        msg.reliable = reliable;

        send(msg, sink);
    }
};


class PhysicalDevice : public PHDBluetoothDevice {
public:
    string displayName, btAddress, btName;
    PhysicalDevice(string displayName, string btAddress, string btName) :
               displayName(displayName), btAddress(btAddress), btName(btName) {}

    string getDisplayName() const noexcept { return displayName; }
    string getBTAddress() const noexcept   { return btAddress;   }
    string getBTName() const noexcept      { return btName;      }
};

class VirtualDevice : public VirtualPHD {
public:
    string displayName;
    uint16_t datatype;
    shared_ptr<PhysicalPHD> physicalDevice;
    VirtualDevice(string displayName, uint16_t datatype,
                  shared_ptr<PhysicalPHD> physicalDevice) :
                                   displayName(displayName), datatype(datatype),
                                   physicalDevice(physicalDevice) {}

    string getDisplayName() const noexcept          { return displayName;     }
    uint16_t getDatatype() const noexcept           { return datatype;        }
    PhysicalPHD &getPhysicalDevice() noexcept       { return *physicalDevice; }
    const PhysicalPHD &getPhysicalDevice() const noexcept
                                                    { return *physicalDevice; }
};




/* ************************************************************ */
/*             Functions invoked from the Java code             */
/* ************************************************************ */


// This object holds all the JNI-related information. It is created,
// maintained and destroyed by javaThread, and only javaThread and
// cppThread may access it.
static JniManager *jniMgr = nullptr;
// Flag indicating that the native methods have already been registered
static bool jniNativesRegistered = false;

static void jniConnectIndication(JNIEnv *env, jobject thiz, jstring address,
                                 jchar datatype, jstring displayName,
                                 jstring serviceName, jstring mdepDescription) {
    assert(jniMgr);
    jniMgr->connectIndication(env, thiz, address, datatype, displayName,
                              serviceName, mdepDescription);
}

static void jniDisconnectIndication(JNIEnv *env, jobject thiz, jstring address,
                                    jchar datatype, jint error) {
    assert(jniMgr);
    jniMgr->disconnectIndication(env, thiz, address, datatype, error);
}

static void jniMessageReceived(JNIEnv *env, jobject thiz, jstring address,
                               jchar datatype, jbyteArray apdu,
                               jboolean reliable) {
    assert(jniMgr);
    jniMgr->messageReceived(env, thiz, address, datatype, apdu, reliable);
}

static void jniRegisterNatives(JNIEnv *env, jclass cls) {
    // Already registered?
    if (jniNativesRegistered) return;

    // Register native/c++ function to handle call from Java
    static const JNINativeMethod methods[] = {
        {"connectIndication", "(Ljava/lang/String;CLjava/lang/String;"
                              "Ljava/lang/String;Ljava/lang/String;)V",
         reinterpret_cast<void *>(jniConnectIndication)},
        {"disconnectIndication", "(Ljava/lang/String;CI)V",
         reinterpret_cast<void *>(jniDisconnectIndication)},
        {"messageReceived", "(Ljava/lang/String;C[BZ)V",
         reinterpret_cast<void *>(jniMessageReceived)}
    };


    jint result = env->RegisterNatives(cls,
                                 methods, sizeof(methods) / sizeof(methods[0]));
    if (result != 0 || env->ExceptionCheck()) {
        ERR("Failed to register native methods");
        assert(false);
    }

    // Flag that we don't have to do this anymore
    jniNativesRegistered = true;
}


} // anonymous namespace




/* ************************************************************ */
/*              PrivateBluetoothModule definition               */
/* ************************************************************ */

class BluetoothModule::PrivateBluetoothModule :
        public MsgReceiver<BluetoothModule::PrivateBluetoothModule,
                           ConnectIndicationMsg>,
        public MsgReceiver<BluetoothModule::PrivateBluetoothModule,
                           DisconnectIndicationMsg>,
        public MsgReceiver<BluetoothModule::PrivateBluetoothModule,
                           ReceiveApduMsg> {
    friend class BluetoothModule;
    friend class MsgReceiver<BluetoothModule::PrivateBluetoothModule,
                             ConnectIndicationMsg>;
    friend class MsgReceiver<BluetoothModule::PrivateBluetoothModule,
                             DisconnectIndicationMsg>;
    friend class MsgReceiver<BluetoothModule::PrivateBluetoothModule,
                             ReceiveApduMsg>;

    // The parent
    BluetoothModule *parent;

    // The states of the module's state machine for subsystem control
    enum { STATE_INACTIVE,     // "Inactive" states
           STATE_STOPPING,
           STATE_CLEANING,

           STATE_STARTING,     // "Active" states
           STATE_RUNNING,
           STATE_RESTARTING,

           STATE_TERMINATING,  // "Killed" states
           STATE_TERMINATED } state;

    // Mutex protecting access to state (which will be modified by both the
    // module thread and javaThread).
    mutex stateLock; 

    // The set of registered datatypes
    set<uint16_t> datatypes;

    // The thread handling Java -> C++ dispatching across the JNI interface.
    // This thread is started by the module's thread when moving into
    // STATE_STARTING. It will shut itself down immediately after transitioning
    // the state from STATE_STOPPING to STATE_CLEANING (or STATE_TERMINATING to
    // STATE_TERMINATED).
    thread *javaThread;

    // The queue of DownstreamRequests - commands going from C++ to Java.
    queue<DownstreamRequest*> requestQueue;

    // Mutex protecting access to requestQueue
    mutex requestQueueLock;

    // Condition variable signalling requests was added to an empty requestQueue
    condition_variable requestQueueHasData;


    // The Physical and Virtual devices we have encountered. Currently, we
    // have to cache them forever, because we have no idea, if anyone has made
    // copies of the proxies of the objects or even the objects' Handles.
    map<string, shared_ptr<PHDBluetoothDevice> > knownPhysicalDevices;
    map<pair<string,uint16_t>, shared_ptr<VirtualPHD> > knownVirtualDevices;
    // "Reverse" mapping of virtual devices we are currently connected to.
    map<FSYS::Handle, pair<string,uint16_t> > connectedDevices;


    PrivateBluetoothModule(BluetoothModule *parent) : parent(parent),
                                    state(STATE_INACTIVE), javaThread(nullptr) {
        assert(Android::getJavaVM());
        DEBUG("Module constructed");
    }


    virtual ~PrivateBluetoothModule() {

        // If the module was already terminated, this is a no-op;
        // otherwise, it will start cleaning up
        terminate();

        // Join and delete the two threads if they exist.
        // Waiting for cleanup, if necessary.
        deleteJavaThread();

        DEBUG("Module destructed");
    }


    // Start the subsystem if it is not already started or the module is
    // terminated. If the module is terminated or if the subsystem is already
    // running, calling this method is a no-op. If the subsystem is currently
    // shutting down, it will restart after it has completed the shut-down.
    void on() {

        {   // Atomic section
            lock_guard<mutex> monitor (stateLock);

            switch (state) {
            case STATE_INACTIVE:
                state = STATE_STARTING;
                break;
            case STATE_STOPPING:
                state = STATE_RESTARTING;
                return;
            case STATE_CLEANING:
                state = STATE_STARTING;
                return;
            default:
                return;
            }
        }

        // If we get to here, we were in the inactive->starting case.
        DEBUG("Bluetooth subsystem STARTING");

        // Join and delete the two threads if they exist.
        // Waiting for cleanup, if necessary.
        deleteJavaThread();

        // Start the javaThread (which will in turn start the cppThread).
        javaThread = new thread(&PAL::BluetoothModule::PrivateBluetoothModule
                                ::javaThreadTask, this);
    }


    // Stop the subsystem if it is running or starting up. Otherwise, calling
    // this method is a no-op.
    void off() {

        {   // Atomic section
            lock_guard<mutex> monitor (stateLock);

            switch (state) {
            case STATE_STARTING:
                state = STATE_CLEANING;
                return;
            case STATE_RESTARTING:
                state = STATE_STOPPING;
                return;
            case STATE_RUNNING:
                state = STATE_STOPPING;
                break;
            default:
                return;
            }
        }

        // If we get to here, we were in the running->stopping case.
        // and we have to turn the Bluetooth subsystem off
        DEBUG("Bluetooth subsystem INTERRUPT");

        interruptSubsystem();
    }


    // Terminate the module, leaving it in a useless zombie state where all
    // incoming signals are ignored. This will also shutdown the subsystem
    // and make sure it can never be restarted.
    void terminate() {

        // Make a note here if previous state was STATE_RUNNING
        // because this means that we have to interrupt the subsystem.
        bool wasRunning = false;

        {   // Atomic section
            lock_guard<mutex> monitor (stateLock);

            switch (state) {
            case STATE_INACTIVE:
                state = STATE_TERMINATED;
                break;
            case STATE_RUNNING:
                wasRunning = true;
                // NO break here!
            case STATE_STARTING:
            case STATE_RESTARTING:
            case STATE_STOPPING:
            case STATE_CLEANING:
                state = STATE_TERMINATING;
                break;
            default: // STATE_TERMINATING, STATE_TERMINATED
                return;
            }
        }

        // If we get to here, we are really terminating
        DEBUG("Module terminating");

        if (wasRunning) {
            // The previous state was STATE_RUNNING, so we need to deregister
            // datatypes and interrupt the subsystem.
            for (uint16_t dt : datatypes) {
                datatypesChanged(dt, false);
            }
            datatypes.clear();
            interruptSubsystem();
        }
    }


    // This is the job executed by the javaThread. Its responsibility
    // is starting the Bluetooth subsystem and the cppThread; communicating
    // Java -> C++ when the subsystem is running (STATE_RUNNING), and shutting
    // down / cleaning up the subsystem after use.
    void javaThreadTask() {

        // Repeat until we succesfully make a transition to STATE_INACTIVE or
        // STATE_TERMINATED
        while (true) {

            // Create the JniManager which holds the Java Bluedroid object
            // which we will be communicating with. The constructor will
            // also attach this thread to the Java VM.
            assert(!jniMgr);
            jniMgr = new JniManager(*this);

            // The queue for C++ -> Java communication should be ready to go
            assert(requestQueue.empty());

            // Transition to the STATE_RUNNING or quit
            bool doRun = false;
            {   // Atomic section
                lock_guard<mutex> monitor (stateLock);

                if (state == STATE_STARTING) {
                    doRun = true;
                    state = STATE_RUNNING;

                    // Transfer the set of datatypes to the queue
                    for (uint16_t dt : datatypes) {
                        datatypesChanged(dt, true);
                    }
                } // else: we are in STATE_CLEANING or STATE_TERMINATING
            }

            // In the following loop, we are in STATE_RUNNING, STATE_STOPPING,
            // STATE_RESTARTING, or STATE_TERMINATING
            while (doRun) {

                // Start the cppThread, which will handle the C++ -> Java
                // communication
                thread *cppThread = new thread(&PAL::BluetoothModule::
                                               PrivateBluetoothModule::
                                               cppThreadTask, this);

                // Execute the Java dispatcher. This will not return until we
                // are in STATE_STOPPING / STATE_RESTARTING / STATE_TERMINATING
                jniMgr->runJavaDispatcher();

                // Wait for the cppThread to complete, so we can clean up

                cppThread->join();
                delete cppThread;

                // Transition back to the STATE_RUNNING or STATE_CLEANING
                {   // Atomic section
                    lock_guard<mutex> monitor (stateLock);

                    doRun = false;
                    if (state == STATE_RESTARTING) {
                        doRun = true;
                        state = STATE_RUNNING;

                        // Transfer the set of datatypes to the queue
                        for (uint16_t dt : datatypes) {
                            datatypesChanged(dt, true);
                        }
                    } else if (state == STATE_STOPPING) {
                        state = STATE_CLEANING;
                    } // else: we are in STATE_TERMINATING
                }
            }

            // When we reach this point, we are either in STATE_CLEANING,
            // STATE_STARTING (treated as clean+loop), or STATE_TERMINATING

            // The queue for C++ -> Java communication should be empty by now
            assert(requestQueue.empty());

            // Destroy the JniManager along with the Java Bluedroid object.
            // The destructor will also detach this thread from the Java VM.
            delete jniMgr;
            jniMgr = nullptr;

            // Final state transition - which may restart the entire thread.
            {   // Atomic section
                lock_guard<mutex> monitor (stateLock);

                if (state == STATE_CLEANING) {
                    state = STATE_INACTIVE;
                    return;
                } else if (state == STATE_TERMINATING) {
                    state = STATE_TERMINATED;
                    return;
                } // else: we are in STATE_STARTING - repeat entire function
            }
        }
    }

    // This is the job executed by the cppThread. Its responsibility is
    // communicating C++ -> Java when the subsystem is running (STATE_RUNNING),
    // and interrupting the javaThread when leaving the STATE_RUNNING state.
    // This thread is started by javaThread right after transitioning the state
    // from STATE_STARTING (or STATE_RESTARTING) into STATE_RUNNING (after it
    // has created the JniManager). It will shut itself down immediately after
    // sending the interrupt signal to the javaThread.
    void cppThreadTask() {

        // Attach this thread to the Java VM
        jniMgr->attachThread();

        // Forward requests from the requestQueue to the JniManager
        // until an InterruptRequest has been forwarded
        while (true) {
            DownstreamRequest *request;

            // Get the next request

            {   // Atomic section
                unique_lock<mutex> monitor (requestQueueLock);

                while (requestQueue.empty()) {
                    requestQueueHasData.wait(monitor);
                }

                request = requestQueue.front();
                requestQueue.pop();
            }

            // Dispatch the request to the JniManager

            if (typeid(*request) == typeid(InterruptRequest)) {
                // Interrupt the javaThread
                jniMgr->interruptReq();
                // Delete the request and quit the cppThread
                delete request;
                break;

            } else if (typeid(*request) == typeid(DatatypeRequest)) {
                DatatypeRequest *dtr = static_cast<DatatypeRequest*>(request);
                jniMgr->datatypeReq(dtr->datatype, dtr->added);

            } else if (typeid(*request) == typeid(DisconnectRequest)) {
                DisconnectRequest *dr =
                                       static_cast<DisconnectRequest*>(request);
                jniMgr->disconnectReq(dr->address, dr->datatype);

            } else if (typeid(*request) == typeid(SendApduRequest)) {
                SendApduRequest *sr = static_cast<SendApduRequest*>(request);
                jniMgr->sendApduReq(sr->address,sr->datatype, move(sr->apdu),
                                    sr->primary);

            } else { // Illegal - should never happen!
                assert(false);
            }

            // Free the request
            delete request;
        }

        // Detach this thread from the Java VM
        jniMgr->detachThread();
    }

    // Join and delete the javaThread. Wait for cleanup if necessary.
    void deleteJavaThread() {
        if (javaThread) {
            if (javaThread->joinable()) javaThread->join();
            delete javaThread;
            javaThread = nullptr;
        }
        assert(!jniMgr);
    }


    void registerDatatype(uint16_t datatype) noexcept {
        DEBUG("registerDatatype() called");
        // Since we have at least one datatype registered, we turn the Bluetooth
        // subsystem on - unless it is already on
        on();
        // We insert the datatype in the set - unless it is already in there
        if (datatypes.insert(datatype).second) {
            // Send the revised datatype to the Bluetooth subsystem
            if (checkRunning()) datatypesChanged(datatype,true);
        }
    }


    void unregisterDatatype(uint16_t datatype) noexcept {
        DEBUG("unregisterDatatype() called");
        // We remove the datatype from the set - unless it is not in there
        if (datatypes.erase(datatype)) {
            // Send the revised datatype to the Bluetooth subsystem
            if (checkRunning()) datatypesChanged(datatype,false);
        }
        if (datatypes.empty()) {
            // No more datatypes registered - turn the Bluetooth subsystem off
            off();
        }
    }



    //
    // The following functions will be called by the module thread to enqueue
    // messages to the cppThread
    //

    // This method is called on the STATE_RUNNING->* transitions. It must only
    // be called when there are no datatypes registered.
    void interruptSubsystem() {
        assert(javaThread && jniMgr && datatypes.empty());

        // Notify the two threads that they must shut down / free the Bluetooth
        // subsystem and then terminate.
        enqueueDownstreamRequest(new InterruptRequest());
    }

    void datatypesChanged(uint16_t datatype, bool added) {
        enqueueDownstreamRequest(new DatatypeRequest(datatype,added));
    }

    void sendApduPrimary(shared_ptr<VirtualPHD> device,
                         shared_ptr<vector<uint8_t> > apdu) noexcept {
        sendApdu(device, apdu, true);
    }

    void sendApduStreaming(shared_ptr<VirtualPHD> device,
                           shared_ptr<vector<uint8_t> > apdu) noexcept {
        sendApdu(device, apdu, false);
    }

    void sendApdu(shared_ptr<VirtualPHD> device,
                  shared_ptr<vector<uint8_t> > apdu, bool primary) noexcept {

        // Check that we are in STATE_RUNNING to ensure the existence of the
        // queue.
        if (!checkRunning()) return;

        // Check that we have a connection to the device
        pair<string,uint16_t> p(lookupConnected(*device));

        if (p.second) {
            // We have a connection!
            // Make a copy of the payload vector
            vector<uint8_t> apduCopy(*apdu);
            // Send payload downstream
            enqueueDownstreamRequest(new SendApduRequest(p.first, p.second,
                                                        apduCopy, primary));
        } else { // Attempting to send a payload to an unconnected device.
            // Log the incident
            WARN("Attempt to send payload to an unconnected device");
        }
    }

    void disconnect(shared_ptr<VirtualPHD> device) noexcept {

        // Check that we are in STATE_RUNNING to ensure the existence of the
        // queue.
        if (!checkRunning()) return;

        // Check that we have a connection to the device
        pair<string,uint16_t> p(lookupConnected(*device));

        if (p.second) {
            // We have a connection! Send disconnect downstream
            enqueueDownstreamRequest(new DisconnectRequest(p.first, p.second));
        } else { // Attempting to disconnect an unconnected device.
            // Log the incident
            WARN("Attempt to disconnect an unconnected device");
        }
    }

    // Check that we are in STATE_RUNNING. This funcion is always invoked
    // on the module thread, and since this is the only thread to ever make
    // transitions away from STATE_RUNNING, a return value of "true" can be
    // trusted to continue to hold without race-conditions.
    bool checkRunning() {
        lock_guard<mutex> monitor (stateLock);

        return state == STATE_RUNNING;
    }


    void enqueueDownstreamRequest(DownstreamRequest *request) {
        // Atomic section
        lock_guard<mutex> monitor (requestQueueLock);

        requestQueue.push(request);

        if (requestQueue.size() == 1) {
            requestQueueHasData.notify_all();
        }
    }



    //
    // The following functions will receive incoming messages from the lower
    // Java layer into the module thread, and forward the messages upstream.
    //

    void receive(ConnectIndicationMsg &msg) {
        // Acquire the VirtualPHD object
        auto vphd = lookupOrCreateVirtual(msg.address, msg.datatype,
                         msg.displayName, msg.serviceName, msg.mdepDescription);

        // Register connectedDevice
        if (connectedDevices.insert(make_pair(FSYS::Handle(*vphd),
                                 make_pair(msg.address,msg.datatype))).second) {
            // Send the connectIndication upstream
            parent->connectIndication(vphd);
        } else {
            // The device was already connected - should not happen...
            // Log the incident
            WARN("connectIndication on already connected device");
        }
    }

    void receive(DisconnectIndicationMsg &msg) {
        auto vphd = lookupVirtual(msg.address,msg.datatype);
        if (vphd) {
            // De-register connectedDevice
            if (connectedDevices.erase(*vphd) == 1) {
                // Send the disconnectIndication upstream
                parent->disconnectIndication(vphd,msg.error);
            } else { // Multiple disconnects - should not happen...
                // Log the incident
                WARN("disconnectIndication on already disconnected device");
            }
        } else { // Disconnect on never-connected device - should not happen...
            // Log the incident
            WARN("disconnectIndication on never-connected device");
        }
    }

    void receive(ReceiveApduMsg &msg) {
        // Lookup the virtual device and check that we have a connection
        auto vphd = lookupVirtual(msg.address,msg.datatype);
        if (vphd && connectedDevices.count(*vphd) == 1) {
            // We have a connection! Prepare the payload.
            shared_ptr<vector<uint8_t> > apdu(new vector<uint8_t>());
            apdu->swap(msg.apdu);
            // Send the payload upstream
            parent->apduReceived(vphd, apdu, msg.reliable);
        } else { // APDU from an unconnected device - should not happen...
            // Log the incident
            WARN("APDU received from an unconnected device");
        }
    }



    //
    // The following functions will handle the creation and lookup of the
    // VirtualPHD objects.
    //

    // Get the physical device object matching the given bluetooth address.
    // Will create the object if there was no match.
    shared_ptr<PHDBluetoothDevice> lookupOrCreatePhysical(string address,
                                       string displayName, string serviceName) {
        auto it = knownPhysicalDevices.find(address);
        if (it == knownPhysicalDevices.end()) {
            // No match: create and store a new physical device
            shared_ptr<PHDBluetoothDevice> dev(new PhysicalDevice(displayName,
                                                          address,serviceName));
            knownPhysicalDevices[address] = dev;
            return dev;
        } else {
            // Return the found device
            return it->second;
        }
    }

    // Get the physical device object matching the given bluetooth address and
    // datatype. Will return an empty shared_ptr if there was no match
    shared_ptr<VirtualPHD> lookupVirtual(string address, uint16_t datatype) {
        auto it = knownVirtualDevices.find(make_pair(address,datatype));
        if (it == knownVirtualDevices.end()) {
            return shared_ptr<VirtualPHD>();
        } else {
            // Return the found device
            return it->second;
        }
    }

    // Get the physical device object matching the given bluetooth address and
    // datatype. Will create the object if there was no match.
    shared_ptr<VirtualPHD> lookupOrCreateVirtual(string address,
                                   uint16_t datatype, string displayName,
                                   string serviceName, string mdepDescription) {
        auto it = knownVirtualDevices.find(make_pair(address,datatype));
        if (it == knownVirtualDevices.end()) {
            // No match: create and store a new virtual device
            shared_ptr<VirtualPHD> dev(
                        new VirtualDevice(mdepDescription,datatype,
                        shared_ptr<PhysicalPHD>(lookupOrCreatePhysical(address,
                                                    displayName,serviceName))));
            knownVirtualDevices[make_pair(address,datatype)] = dev;
            return dev;
        } else {
            // Return the found device
            return it->second;
        }
    }

    // Get the address+datatype matching the given VirtualPHD handle of a
    // device, we currently have an open connection to. If we have no open
    // connection to this Handle, return ("",0)
    pair<string,uint16_t> lookupConnected(FSYS::Handle &virtualDevice) {
        auto it = connectedDevices.find(virtualDevice);
        if (it == connectedDevices.end()) {
            return std::make_pair("",0);
        } else {
            return it->second;
        }
    }
};




/* ************************************************************ */
/*                BluetoothModule implementation                */
/* ************************************************************ */

// The BluetoothModule ctor/dtor creates and deletes the PrivateBluetoothModule
// defined earlier. All methods of the BluetoothModule are then simply forwarded
// to the private counterpart.

BluetoothModule::BluetoothModule() :
    privateParts (new PrivateBluetoothModule(this)) {}

BluetoothModule::~BluetoothModule() {
    assert(privateParts);
    delete privateParts;
    privateParts = nullptr;
}

void BluetoothModule::msgGoingDown() {
    assert(privateParts);
    privateParts->terminate();
}

void BluetoothModule::registerDatatype(uint16_t datatype) noexcept {
    assert(privateParts);
    privateParts->registerDatatype(datatype);
}

void BluetoothModule::unregisterDatatype(uint16_t datatype) noexcept {
    assert(privateParts);
    privateParts->unregisterDatatype(datatype);
}

void BluetoothModule::sendApduPrimary(shared_ptr<VirtualPHD> device,
                                   shared_ptr<vector<uint8_t> > apdu) noexcept {
    assert(privateParts);
    privateParts->sendApduPrimary(device, apdu);
}

void BluetoothModule::sendApduStreaming(shared_ptr<VirtualPHD> device,
                                   shared_ptr<vector<uint8_t> > apdu) noexcept {
    assert(privateParts);
    privateParts->sendApduStreaming(device, apdu);
}

void BluetoothModule::disconnect(shared_ptr<VirtualPHD> device) noexcept {
    assert(privateParts);
    privateParts->disconnect(device);
}

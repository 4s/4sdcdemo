/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief PAL layer abstract classes for the personal health device
 *        communication interface.
 *
 * This file contains the implementation of the abstract handler and provider
 * classes for the PAL::PersonalHealthDeviceConnector:
 * PAL::PersonalHealthDeviceHandler, PAL::PersonalHealthDeviceProviderBase,
 * and derived classes (PAL::PHDBluetoothProvider).
 *
 * @todo The following paragraph should probably be moved somewhere else.
 *
 * When a handler registers a datatype using
 * PersonalHealthDeviceHandler::registerDatatype(), the current implementation
 * will register this handler with all *existing* providers. If a provider is
 * instantiated *after* some handler has registered its datatype, the provider
 * will not use this handler (unless it unregisters the datatype and registers
 * it again). This behaviour *may* change in the future!
 *
 * @see personalhealthdevice.h
 * @see PAL::PersonalHealthDeviceConnector
 * @see PAL::PersonalHealthDeviceHandler
 * @see PAL::PersonalHealthDeviceProviderBase
 * @see PAL::PHDBluetoothProvider
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */


#include "PAL/personalhealthdevice.h"
#include "FSYS/msgsender.h"
#include "FSYS/msgreceiver.h"

#include <cassert>
#include <map>
#include <forward_list>
#include <functional>
#include <tuple>
#include <utility>


using FSYS::Handle;
using FSYS::MsgAddr;
using FSYS::MsgReceiver;
using FSYS::MsgSender;

using PAL::VirtualPHD;
using PAL::PersonalHealthDeviceHandler;
using PAL::PersonalHealthDeviceProviderBase;
using PAL::PhysicalPHD;
using PAL::PHDBluetoothDevice;
using PAL::PHDBluetoothProvider;
using PAL::PHDConnectIndicationMsg;
using PAL::PHDRegisterDatatypeMsg;
using PAL::PHDUSBDevice;
using PAL::PHDZigBeeDevice;

using PAL::PHDRegisterDatatypeMsg;
using PAL::PHDUnregisterDatatypeMsg;
using PAL::PHDConnectIndicationMsg;
using PAL::PHDDisconnectIndicationMsg;
using PAL::PHDDataTransferMsg;
using PAL::PHDDisconnectMsg;
using PAL::PHDConnectIndicationBluetoothMsg;
using PAL::PHDConnectIndicationUSBMsg;
using PAL::PHDConnectIndicationZigBeeMsg;


using std::forward_list;
using std::function;
using std::get;
using std::make_pair;
using std::make_tuple;
using std::map;
using std::pair;
using std::shared_ptr;
using std::string;
using std::tuple;
using std::vector;




/* ************************************************************ */
/*   Virtual & PhysicalPHD proxy classes used by the handler    */
/* ************************************************************ */

// We keep them private to this file...
namespace {

// The proxies are instantiated using the static methods newGeneric, newBT,
// newUSB, and newZigBee found at the bottom.
class PHDProxy : public VirtualPHD {
private:
    uint16_t datatype;
    string virtualName;
    PhysicalPHD *physicalProxy;

    // The constructor is private as all construction use static methods
    PHDProxy(Handle &vHandle, uint16_t datatype,
             const string vName, PhysicalPHD *pProxy) :
                    VirtualPHD(vHandle), datatype(datatype), virtualName(vName),
                    physicalProxy(pProxy) {}

    // A generic PhysicalPHD
    class GenericPhysicalProxy : public PhysicalPHD {
    private:
        string physicalName;
    public:
        GenericPhysicalProxy(Handle &h, const string pName) :
                                          PhysicalPHD(h), physicalName(pName) {}
        std::string getDisplayName() const noexcept { return physicalName; }
    };

    // The Bluetooth specialization of the PhysicalPHD
    class BluetoothPhysicalProxy : public PHDBluetoothDevice {
    private:
        string physicalName, btAddress, btName;
    public:
        BluetoothPhysicalProxy(Handle &h, const string pName,
                               const string btAddress, const string btName) :
                            PHDBluetoothDevice(h), physicalName(pName),
                            btAddress(btAddress), btName(btName) {}

        std::string getDisplayName() const noexcept { return physicalName; }
        std::string getBTAddress() const noexcept { return btAddress; }
        std::string getBTName() const noexcept { return btName; }
    };

    // The USB specialization of the PhysicalPHD
    class USBPhysicalProxy : public PHDUSBDevice {
    private:
        string physicalName;
    public:
        USBPhysicalProxy(Handle &h, const string pName) :
                                         PHDUSBDevice(h), physicalName(pName) {}
        std::string getDisplayName() const noexcept { return physicalName; }
    };

    // The ZigBee specialization of the PhysicalPHD
    class ZigBeePhysicalProxy : public PHDZigBeeDevice {
    private:
        string physicalName;
    public:
        ZigBeePhysicalProxy(Handle &h, const string pName) :
                                      PHDZigBeeDevice(h), physicalName(pName) {}
        std::string getDisplayName() const noexcept { return physicalName; }
    };

public:
    ~PHDProxy() { delete physicalProxy; }
    PhysicalPHD &getPhysicalDevice() noexcept { return *physicalProxy; }
    const PhysicalPHD &getPhysicalDevice() const noexcept
                                                      { return *physicalProxy; }
    uint16_t getDatatype() const noexcept { return datatype; }
    string getDisplayName() const noexcept { return virtualName; }

    // Create a new VirtualPHD associated to a generic (non-hardware-specific)
    // PhysicalPHD
    static PHDProxy *newGeneric(Handle &vHandle, Handle &pHandle,
                                uint16_t datatype, const string vName,
                                const string pName) {
        auto pphd = new GenericPhysicalProxy(pHandle, pName);
        return new PHDProxy(vHandle,datatype,vName,pphd);
    }

    // Create a new VirtualPHD associated to a Bluetooth device
    static PHDProxy *newBluetooth(Handle &vHandle, Handle &pHandle,
                                  uint16_t datatype, const string vName,
                                  const string pName, const string btAddress,
                                  const string btName) {
        auto pphd = new BluetoothPhysicalProxy(pHandle,pName,btAddress,btName);
        return new PHDProxy(vHandle,datatype,vName,pphd);
    }

    // Create a new VirtualPHD associated to a USB device
    static PHDProxy *newUSB(Handle &vHandle, Handle &pHandle, uint16_t datatype,
                            const string vName, const string pName) {
        auto pphd = new USBPhysicalProxy(pHandle, pName);
        return new PHDProxy(vHandle,datatype,vName,pphd);
    }

    // Create a new VirtualPHD associated to a ZigBee device
    static PHDProxy *newZigBee(Handle &vHandle, Handle &pHandle,
                               uint16_t datatype, const string vName,
                               const string pName) {
        auto pphd = new ZigBeePhysicalProxy(pHandle, pName);
        return new PHDProxy(vHandle,datatype,vName,pphd);
    }
};



// TODO: document and move to a different section
// (keeping it in anonymous namespace)
class Eraser {
private:
    function<void ()> eraseTask;
public:
    ~Eraser() { if (eraseTask) eraseTask(); }
    inline void setEraseTask(function<void ()> u) noexcept { eraseTask = u; }
};

} // anonymous namespace




/* ************************************************************ */
/*                 PersonalHealthDeviceHandler                  */
/* ************************************************************ */


class PersonalHealthDeviceHandler::PrivatePHDHandler :
        public MsgSender<PersonalHealthDeviceHandler::PrivatePHDHandler>,
        public MsgReceiver<PersonalHealthDeviceHandler::PrivatePHDHandler,
                           PHDConnectIndicationMsg>,
        public MsgReceiver<PersonalHealthDeviceHandler::PrivatePHDHandler,
                           PHDDisconnectIndicationMsg>,
        public MsgReceiver<PersonalHealthDeviceHandler::PrivatePHDHandler,
                           PHDDataTransferMsg>,
        public MsgReceiver<PersonalHealthDeviceHandler::PrivatePHDHandler,
                           PHDConnectIndicationBluetoothMsg>,
        public MsgReceiver<PersonalHealthDeviceHandler::PrivatePHDHandler,
                           PHDConnectIndicationUSBMsg>,
        public MsgReceiver<PersonalHealthDeviceHandler::PrivatePHDHandler,
                           PHDConnectIndicationZigBeeMsg> {

    // The public counterpart is of course a friend
    friend class PersonalHealthDeviceHandler;

public:
    // Methods receiving the messages from the PAL component
    void receive(PHDConnectIndicationMsg msg);
    void receive(PHDDisconnectIndicationMsg msg);
    void receive(PHDDataTransferMsg msg);
    void receive(PHDConnectIndicationBluetoothMsg msg);
    void receive(PHDConnectIndicationUSBMsg msg);
    void receive(PHDConnectIndicationZigBeeMsg msg);


private:
    // The public counterpart
    PersonalHealthDeviceHandler * const parent;

    // The datatypes, this handler will currently accept
    map<uint16_t, Eraser> registeredDatatypes;

    // The ongoing conversations; mapping the Handle used as "device"
    // in the messages to the PersonalHealthDevice object and the
    // message we must use to talk back to this device.
    map<FSYS::Handle, tuple<shared_ptr<VirtualPHD>,
                            MsgAddr,
                            Eraser> > conversations;


    PrivatePHDHandler(PersonalHealthDeviceHandler *parent);
    virtual ~PrivatePHDHandler();


    // Mirroring the public interface of the public counterpart
    void registerDatatype(uint16_t datatype);
    void unregisterDatatype(uint16_t datatype) noexcept;
    void sendApduPrimary(shared_ptr<VirtualPHD> device,
                                 shared_ptr<vector<uint8_t> > apdu) noexcept;
    void disconnect(shared_ptr<VirtualPHD> device) noexcept;
    void sendApduStreaming(shared_ptr<VirtualPHD> device,
                                 shared_ptr<vector<uint8_t> > apdu) noexcept;


    void receive(PHDConnectIndicationMsg msg,
                 shared_ptr<VirtualPHD> device);

};



//
// The ctor/dtor and methods of the public interface (relay to private)
//

PersonalHealthDeviceHandler::PersonalHealthDeviceHandler() :
    privateParts(new PrivatePHDHandler(this)) {}


PersonalHealthDeviceHandler::~PersonalHealthDeviceHandler() {
    delete privateParts;
}

void PersonalHealthDeviceHandler::registerDatatype(uint16_t datatype) {
    assert(privateParts);
    privateParts->registerDatatype(datatype);
}

void PersonalHealthDeviceHandler::unregisterDatatype(uint16_t datatype)
                                                                      noexcept {
    assert(privateParts);
    privateParts->unregisterDatatype(datatype);
}

void PersonalHealthDeviceHandler::sendApduPrimary(shared_ptr<VirtualPHD> device,
                                shared_ptr<vector<uint8_t> > apdu) noexcept {
    assert(privateParts);
    privateParts->sendApduPrimary(device,apdu);
}

void PersonalHealthDeviceHandler::disconnect(
                             shared_ptr<VirtualPHD> device) noexcept {
    assert(privateParts);
    privateParts->disconnect(device);
}

void PersonalHealthDeviceHandler::sendApduStreaming(shared_ptr<VirtualPHD> device,
                                shared_ptr<vector<uint8_t> > apdu) noexcept {
    assert(privateParts);
    privateParts->sendApduStreaming(device,apdu);
}



//
// The implementation of the private class
//

PersonalHealthDeviceHandler::PrivatePHDHandler::
      PrivatePHDHandler(PersonalHealthDeviceHandler *parent) : parent(parent) {}


PersonalHealthDeviceHandler::PrivatePHDHandler::~PrivatePHDHandler() {

    // Unregister any remaining registeredDatatypes, automagically broadcasting
    // notifications that we will no longer handle these datatypes.
    registeredDatatypes.clear();

    // Send PHDDisconnectMsg for any remaining conversations and clean up.
    conversations.clear();
}


void PersonalHealthDeviceHandler::PrivatePHDHandler::
                                           registerDatatype(uint16_t datatype) {

    // Attempt to insert the datatype in the map.
    // In theory, insert() may throw some kind of exception (probably just
    // std::bad_alloc as 'out of memory'), but if insert() succeeds, no
    // exception can be thrown.
    auto result = registeredDatatypes.insert(make_pair(datatype,Eraser()));
    if (result.second) {
        // Success: we were not already listening to this datatype
        // Broadcast the announcement: We will handle this datatype ...
        PHDRegisterDatatypeMsg m;
        m.datatype = datatype;
        broadcast(m);
        // ... and whenever the element is removed from the map in the future,
        // broadcast the announcement: We will no longer handle this datatype.
        result.first->second.setEraseTask([this,datatype]() {
            PHDUnregisterDatatypeMsg m;
            m.datatype = datatype;
            broadcast(m);
        });
    }
}


void PersonalHealthDeviceHandler::PrivatePHDHandler::
                                unregisterDatatype(uint16_t datatype) noexcept {
    // Attempt to remove the datatype from the set. If successful, an
    // unregistration message (PHDUnregisterDatatypeMsg) will be broadcast.
    registeredDatatypes.erase(datatype);
}


void PersonalHealthDeviceHandler::PrivatePHDHandler::
             sendApduPrimary(shared_ptr<VirtualPHD> device,
                                shared_ptr<vector<uint8_t> > apdu) noexcept {
    // Lookup the device in our list of ongoing conversations
    auto conversation(conversations.find(*device));
    if (conversation == conversations.end()) {
        // No conversation found: Ignore the message
        return;
    }

    // Retrieve the destination object
    auto destination(get<1>(conversation->second));

    // Build the message and send it
    PHDDataTransferMsg msg;
    msg.virtualDevice = *device;
    msg.apdu = *apdu;
    msg.reliable = true;
    send(msg,destination);
}


void PersonalHealthDeviceHandler::PrivatePHDHandler::
                  disconnect(shared_ptr<VirtualPHD> device) noexcept {
    // Attemt to remove the device from the conversations map. If successful,
    // a disconnect message (PHDDisconnectMsg) will be sent automatically.
    conversations.erase(*device);
}


void PersonalHealthDeviceHandler::PrivatePHDHandler::
           sendApduStreaming(shared_ptr<VirtualPHD> device,
                                shared_ptr<vector<uint8_t> > apdu) noexcept {
    // Lookup the device in our list of ongoing conversations
    auto conversation(conversations.find(*device));
    if (conversation == conversations.end()) {
        // No conversation found: Ignore the message
        return;
    }

    // Retrieve the PersonalHealthDevice object
    auto dev(get<0>(conversation->second));
    // And test if it is a PHDBluetoothDevice
    if (dynamic_cast<PHDBluetoothDevice*>(dev.get()) == nullptr) {
        // If not; use the primary channel instead
        return sendApduPrimary(device,apdu);
    }

    // We are communicating with a PHDBluetoothDevice

    // Retrieve the destination object
    auto destination(get<1>(conversation->second));

    // Build the message and send it
    PHDDataTransferMsg msg;
    msg.virtualDevice = *device;
    msg.apdu = *apdu;
    msg.reliable = false;
    send(msg,destination);
}


void PersonalHealthDeviceHandler::PrivatePHDHandler::
                                          receive(PHDConnectIndicationMsg msg) {

    // Create a suitable PersonalHealthDevice...
    shared_ptr<VirtualPHD>phd(PHDProxy::newGeneric(
                                  msg.virtualDevice, msg.physicalDevice,
                                  msg.datatype, msg.virtualDisplayname,
                                  msg.physicalDisplayname));
    // ... and continue with the common receive method
    receive(msg, phd);
}


void PersonalHealthDeviceHandler::PrivatePHDHandler::
                              receive(PHDConnectIndicationMsg msg,
                                      shared_ptr<VirtualPHD> device) {
    // Check if we want to receive this connection
    if (registeredDatatypes.count(msg.datatype) == 0) {
        // We are not interested - we do not listen for this datatype right now.
        // (If everything is implemented correctly, this path should only be
        // possible in case of a race-condition, in which case a
        // PHDUnregisterDatatypeMsg message is already on its way to the
        // sender of this PHDConnectIndicationMsg message).
        return;
    }

    // We are willing to accept the connection
    // (but we need to check that we are not already connected)

    // We will store a copy of the destination address
    MsgAddr destination(msg.getOriginAddr());

    // Attempt to initiate a conversation with this peer.
    auto result = conversations.insert(make_pair(FSYS::Handle(*device),
                                        make_tuple(device,destination,Eraser())));
    if (result.second) {
        // Success: we were not already connected to this peer
        // Setup an automated disconnect message when this conversation is
        // removed from the list of conversations
        get<2>(result.first->second).setEraseTask([this,device,destination]() {
            parent->disconnectIndication(device, 0);
        });

        // Signal the new connection to the child-object.
        parent->connectIndication(device);
    }
}


void PersonalHealthDeviceHandler::PrivatePHDHandler::
                                       receive(PHDDisconnectIndicationMsg msg) {
    // Lookup the device in our list of ongoing conversations
    auto conversation(conversations.find(msg.virtualDevice));
    if (conversation == conversations.end()) {
        // No conversation found: Ignore the message
        return;
    }

    // We are going to delete the conversation but we don't want to send the
    // automatic disconnect message. So we first "disarm" the Eraser
    get<2>(conversation->second).setEraseTask(nullptr);
    // and retrieve the PersonalHealthDevice object
    auto device(get<0>(conversation->second));

    // Now we can delete the conversation
    conversations.erase(msg.virtualDevice);

    // Signal the disconnection to the child-object.
    parent->disconnectIndication(device, msg.error);
}


void PersonalHealthDeviceHandler::PrivatePHDHandler::
                                               receive(PHDDataTransferMsg msg) {
    // Lookup the device in our list of ongoing conversations
    auto conversation(conversations.find(msg.virtualDevice));
    if (conversation == conversations.end()) {
        // No conversation found: Ignore the message
        return;
    }

    // Retrieve the PersonalHealthDevice object
    auto device(get<0>(conversation->second));

    // and the APDU
    shared_ptr<vector<uint8_t> > m(new vector<uint8_t>(msg.apdu));

    // Signal the reception to the child-object.
    parent->apduReceived(device, m, msg.reliable);
}


void PersonalHealthDeviceHandler::PrivatePHDHandler::
                                 receive(PHDConnectIndicationBluetoothMsg msg) {
    // Create a suitable PersonalHealthDevice for a bluetooth device...
    shared_ptr<VirtualPHD>phd(PHDProxy::newBluetooth(
                                  msg.virtualDevice, msg.physicalDevice,
                                  msg.datatype, msg.virtualDisplayname,
                                  msg.physicalDisplayname, msg.bluetoothAddress,
                                  msg.bluetoothName));
    // ... and continue with the common receive method
    receive(msg,phd);
}


void PersonalHealthDeviceHandler::PrivatePHDHandler::
                                       receive(PHDConnectIndicationUSBMsg msg) {
    // Create a suitable PersonalHealthDevice for a USB device...
    shared_ptr<VirtualPHD>phd(PHDProxy::newUSB(
                                  msg.virtualDevice, msg.physicalDevice,
                                  msg.datatype, msg.virtualDisplayname,
                                  msg.physicalDisplayname));
    // ... and continue with the common receive method
    receive(msg,phd);
}


void PersonalHealthDeviceHandler::PrivatePHDHandler::
                                    receive(PHDConnectIndicationZigBeeMsg msg) {
    // Create a suitable PersonalHealthDevice for a ZigBee device...
    shared_ptr<VirtualPHD>phd(PHDProxy::newZigBee(
                                  msg.virtualDevice, msg.physicalDevice,
                                  msg.datatype, msg.virtualDisplayname,
                                  msg.physicalDisplayname));
    // ... and continue with the common receive method
    receive(msg,phd);
}




/* ************************************************************ */
/*               PersonalHealthDeviceProviderBase               */
/* ************************************************************ */

class PersonalHealthDeviceProviderBase::PrivatePHDProvider :
        public MsgSender<PersonalHealthDeviceProviderBase::PrivatePHDProvider>,
        public MsgReceiver<PersonalHealthDeviceProviderBase::PrivatePHDProvider,
                           PHDRegisterDatatypeMsg>,
        public MsgReceiver<PersonalHealthDeviceProviderBase::PrivatePHDProvider,
                           PHDUnregisterDatatypeMsg>,
        public MsgReceiver<PersonalHealthDeviceProviderBase::PrivatePHDProvider,
                           PHDDataTransferMsg>,
        public MsgReceiver<PersonalHealthDeviceProviderBase::PrivatePHDProvider,
                           PHDDisconnectMsg> {

    // The public counterpart is of course a friend
    friend class PersonalHealthDeviceProviderBase;

public:
    // Methods receiving the messages from the PAL component
    void receive(PHDRegisterDatatypeMsg msg);
    void receive(PHDUnregisterDatatypeMsg msg);
    void receive(PHDDataTransferMsg msg);
    void receive(PHDDisconnectMsg msg);


private:
    // The public counterpart
    PersonalHealthDeviceProviderBase * const parent;

    // The handlers currently registered (by datatype)
    map<uint16_t, forward_list<shared_ptr<PHDRegisterDatatypeMsg> > > peers;

    // The ongoing conversations; mapping the Handle used as "device"
    // in the messages to the PersonalHealthDevice object and the
    // message we must use to talk back to this device.
    map<FSYS::Handle, tuple<shared_ptr<VirtualPHD>,
                            MsgAddr,
                            Eraser> > conversations;


    PrivatePHDProvider(PersonalHealthDeviceProviderBase *parent);
    virtual ~PrivatePHDProvider();


    // Mirroring the public interface of the public counterpart
    void connectIndication(shared_ptr<VirtualPHD> device) noexcept;
    void disconnectIndication(shared_ptr<VirtualPHD> device,
                              errortype error) noexcept;
    void apduReceived(shared_ptr<VirtualPHD> device,
                         shared_ptr<vector<uint8_t> > apdu,
                         bool reliableTransport) noexcept;

};



//
// The ctor/dtor and methods of the public interface (relay to private)
//

PersonalHealthDeviceProviderBase::PersonalHealthDeviceProviderBase() :
    privateParts(new PrivatePHDProvider(this)) {}


PersonalHealthDeviceProviderBase::~PersonalHealthDeviceProviderBase() {
    delete privateParts;
}

void PersonalHealthDeviceProviderBase::connectIndication(
                             shared_ptr<VirtualPHD> device) noexcept {
    assert(privateParts);
    privateParts->connectIndication(device);
}

void PersonalHealthDeviceProviderBase::disconnectIndication(
                                        shared_ptr<VirtualPHD> device,
                                        errortype error) noexcept {
    assert(privateParts);
    privateParts->disconnectIndication(device,error);
}

void PersonalHealthDeviceProviderBase::apduReceived(
                                shared_ptr<VirtualPHD> device,
                                shared_ptr<vector<uint8_t> > apdu,
                                bool reliableTransport) noexcept {
    assert(privateParts);
    privateParts->apduReceived(device,apdu,reliableTransport);
}



//
// The implementation of the private class
//

PersonalHealthDeviceProviderBase::PrivatePHDProvider::PrivatePHDProvider(
                   PersonalHealthDeviceProviderBase *parent) : parent(parent) {}


PersonalHealthDeviceProviderBase::PrivatePHDProvider::~PrivatePHDProvider() {

    // Send PHDDisconnectIndicationMsg for any remaining conversations with
    // en error indication and clean up.
    conversations.clear();
}


void PersonalHealthDeviceProviderBase::PrivatePHDProvider::
           connectIndication(shared_ptr<VirtualPHD> device) noexcept {

    // Check if we know of any handler which will handle this datatype
    auto peerCandidates(peers.find(device->getDatatype()));
    if (peerCandidates == peers.end() || peerCandidates->second.empty()) {
        // No candidate list found, return
        return;
    }

    // As the candidate list is non-empty, we will use its fist element as peer
    // for this conversation.

    // Make a clone of the PHDRegisterDatatypeMsg message
    MsgAddr destination(peerCandidates->second.front()->getOriginAddr());

    // Attempt to initiate a conversation with this virtual device.
    auto result = conversations.insert(make_pair(FSYS::Handle(*device),
                                        make_tuple(device,destination,Eraser())));
    if (result.second) {
        // Success: we were not already connected to this virtual device
        // Setup an automated disconnect indication when this conversation is
        // removed from the list of conversations
        get<2>(result.first->second).setEraseTask([this,device,destination]() {
            PHDDisconnectIndicationMsg m;
            m.virtualDevice = *device;
            m.error = 42; // FIXME: errorcode???
            send(m,destination);
        });

        // Signal the new connection to the peer.

        // Copy data from a generic PhysicalPHD to a PHDConnectIndicationMsg
        auto fillOut([device](PHDConnectIndicationMsg &m) {
            m.virtualDevice = *device;
            m.physicalDevice = device->getPhysicalDevice();
            m.datatype = device->getDatatype();
            m.virtualDisplayname = device->getDisplayName();
            m.physicalDisplayname = device->getPhysicalDevice().
                                                               getDisplayName();
        });

        if (dynamic_cast<PHDBluetoothDevice*>(&device->getPhysicalDevice())) {
            PHDBluetoothDevice &d = dynamic_cast<PHDBluetoothDevice&>
                                                  (device->getPhysicalDevice());
            PHDConnectIndicationBluetoothMsg m;
            m.bluetoothAddress = d.getBTAddress();
            m.bluetoothName = d.getBTName();
            fillOut(m);
            send(m,destination);
        } else if (dynamic_cast<PHDUSBDevice*>(&device->getPhysicalDevice())) {
            PHDUSBDevice &d = dynamic_cast<PHDUSBDevice&>
                                                  (device->getPhysicalDevice());
            (void) d; // Currently no USB specialization implemented
            PHDConnectIndicationUSBMsg m;
            fillOut(m);
            send(m,destination);
        } else if (dynamic_cast<PHDZigBeeDevice*>
                                               (&device->getPhysicalDevice())) {
            PHDZigBeeDevice &d = dynamic_cast<PHDZigBeeDevice&>
                                                  (device->getPhysicalDevice());
            (void) d; // Currently no ZigBee specialization implemented
            PHDConnectIndicationZigBeeMsg m;
            fillOut(m);
            send(m,destination);
        } else { // Just a generic PhysicalPHD
            PHDConnectIndicationMsg m;
            fillOut(m);
            send(m,destination);
        }
    }
}


void PersonalHealthDeviceProviderBase::PrivatePHDProvider::
                   disconnectIndication(shared_ptr<VirtualPHD> device,
                                        errortype error) noexcept {
    // Lookup the device in our list of ongoing conversations
    auto conversation(conversations.find(*device));
    if (conversation == conversations.end()) {
        // No conversation found: Ignore the message
        return;
    }

    // Retrieve the destination object
    auto destination(get<1>(conversation->second));

    // We are going to delete the conversation but we don't want to send the
    // automatic disconnect indication. So we first "disarm" the Eraser
    get<2>(conversation->second).setEraseTask(nullptr);

    // Now we can delete the conversation
    conversations.erase(*device);

    // Build the message and send it
    PHDDisconnectIndicationMsg msg;
    msg.virtualDevice = *device;
    msg.error = error;
    send(msg,destination);
}


void PersonalHealthDeviceProviderBase::PrivatePHDProvider::
                        apduReceived(shared_ptr<VirtualPHD> device,
                                        shared_ptr<vector<uint8_t> > apdu,
                                        bool reliableTransport) noexcept {
    // Lookup the device in our list of ongoing conversations
    auto conversation(conversations.find(*device));
    if (conversation == conversations.end()) {
        // No conversation found: Ignore the message
        return;
    }

    // Retrieve the destination object
    auto destination(get<1>(conversation->second));

    // Build the message and send it
    PHDDataTransferMsg msg;
    msg.virtualDevice = *device;
    msg.apdu = *apdu;
    msg.reliable = reliableTransport;
    send(msg,destination);
}


void PersonalHealthDeviceProviderBase::PrivatePHDProvider::
                                           receive(PHDRegisterDatatypeMsg msg) {

    // If we are currently not receiving this datatype, make a new list of
    // handlers for this datatype
    auto result = peers.insert(make_pair(msg.datatype,
                          forward_list<shared_ptr<PHDRegisterDatatypeMsg> >()));

    // Make a clone of the PHDRegisterDatatypeMsg message to store for later
    shared_ptr<PHDRegisterDatatypeMsg> respondTo(
                                               new PHDRegisterDatatypeMsg(msg));

    // Add the handler to the *front* of the list (last-registered-handler-is-
    // used semantics).
    result.first->second.emplace_front(respondTo);

    // Was this the first registered handler for this datatype?
    if (result.second) {
        // This was the first handler to handle this particular datatype.
        // Tell the child-object
        parent->registerDatatype(msg.datatype);
    }
}


void PersonalHealthDeviceProviderBase::PrivatePHDProvider::
                                         receive(PHDUnregisterDatatypeMsg msg) {
    // map<uint16_t, forward_list<shared_ptr<PHDRegisterDatatypeMsg> > > peers;
    // Check if we know of any handler which will handle this datatype
    auto peerCandidates(peers.find(msg.datatype));
    if (peerCandidates == peers.end()) {
        // No candidate list found, return
        return;
    }

    // A candidate list was found. Remove the handler from this list (if it is
    // on it).
    peerCandidates->second.remove_if(
                [msg](shared_ptr<PHDRegisterDatatypeMsg> m){
                    // Are the senders m and msg identical
                    return msg.getOriginAddr() == m->getOriginAddr();
                });

    // Check if the list became empty
    if (peerCandidates->second.empty()) {
        // Remove from the map
        peers.erase(peerCandidates);
        // ...and tell the child-object that noone will handle this datatype
        parent->unregisterDatatype(msg.datatype);
    }
}


void PersonalHealthDeviceProviderBase::PrivatePHDProvider::
                                               receive(PHDDataTransferMsg msg) {
    // Lookup the device in our list of ongoing conversations
    auto conversation(conversations.find(msg.virtualDevice));
    if (conversation == conversations.end()) {
        // No conversation found: Ignore the message
        return;
    }

    // Retrieve the PersonalHealthDevice object
    auto device(get<0>(conversation->second));

    // and the APDU
    shared_ptr<vector<uint8_t> > m(new vector<uint8_t>(msg.apdu));


    // Determine the type of this provider
    if (dynamic_cast<PHDBluetoothProvider*>(parent)) {
        // Bluetooth
        if (!msg.reliable) {
            // Send the APDU using the streaming channel
            dynamic_cast<PHDBluetoothProvider*>(parent)->
                                                sendApduStreaming(device, m);
            return;
        }
    }

    // Send the APDU to the child-object using the primary channel
    // if no alternative was used above.
    parent->sendApduPrimary(device, m);
}


void PersonalHealthDeviceProviderBase::PrivatePHDProvider::
                                                 receive(PHDDisconnectMsg msg) {
    // Lookup the device in our list of ongoing conversations
    auto conversation(conversations.find(msg.virtualDevice));
    if (conversation == conversations.end()) {
        // No conversation found: Ignore the message
        return;
    }

    // Retrieve the PersonalHealthDevice object
    auto device(get<0>(conversation->second));

    // Signal the disconnection request to the child-object.
    parent->disconnect(device);
}
